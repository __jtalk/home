/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.rs.endpoint

import javax.ws.rs.{GET, Path, Produces, QueryParam}
import java.util.{ArrayList => JArrayList, List => JList}
import javax.annotation.ManagedBean
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.servlet.http.HttpServletRequest
import javax.ws.rs.core.{Context, Request}

import me.jtalk.home.service.search.{SearchResult, SearchService}

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

@ManagedBean
@Path("/search")
@ApplicationScoped
class SearchEndpoint {

	@Inject
	private var searcher: SearchService = _

	@GET
	@Produces(Array("application/json"))
	def search(@QueryParam("q") query: String, @Context request: HttpServletRequest): JArrayList[SearchResult] = {
		val raw = searcher.previewSearch(query)
		val prepended = raw.map(r => SearchResult(r.header, prependContextPath(ctxPath(request), r.url), r.smallPreview, r.largePreview))
		new JArrayList(prepended)
	}

	protected def prependContextPath(contextPath: => String, url: String): String = Option(url)
	        .map(contextPath + _)
	        .orNull

	protected def ctxPath(request: HttpServletRequest): String = request.getContextPath
}
