/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.utils

import javax.ejb.{TransactionAttributeType, TransactionAttribute, Stateless}

import com.codahale.metrics.annotation.{Timed, Metered}
import org.pegdown.PegDownProcessor

@Stateless
class MarkdownProcessor {

	private val processor = new PegDownProcessor()

	@Timed(name = "markdown.processor.timer", absolute = true)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	def toHtml(src: String): String = processor.markdownToHtml(src)
}
