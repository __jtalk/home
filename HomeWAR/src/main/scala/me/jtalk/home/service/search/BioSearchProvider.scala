/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service.search

import javax.annotation.{ManagedBean, PostConstruct}
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.event.{Observes, Reception, TransactionPhase}
import javax.inject.Inject

import com.codahale.metrics.annotation.{Timed, Metered}
import me.jtalk.home.faces.utils.{BioUtils, Urls}
import me.jtalk.home.service.event.BioUpdated
import me.jtalk.home.utils.SearchContentPurifier

@ManagedBean
@ApplicationScoped
class BioSearchProvider extends SearchProvider {

	@Inject
	private var bio: BioUtils = _

	@Inject
	private var purifier: SearchContentPurifier = _

	@Inject
	private var cutter: SearchPreviewCutter = _

	private var contentCache: String = _

	@PostConstruct
	def load(): Unit = {
		contentCache = purifier.purify(bio.content)
	}

	@Timed(name = "search.bio.timer", absolute = true)
	override def search(queryRaw: String): List[SearchResult] = {
		val query = purifier.purifyPlain(queryRaw)
		val results = Stream(
			SearchProvider.findIfExistsField(query, purifier.purifyPlain(bio.ownerEmail), Urls.BIO_URL, "Bio", identity),
			SearchProvider.findIfExistsField(query, purifier.purifyPlain(bio.ownerName), Urls.BIO_URL, "Bio", identity),
			SearchProvider.findIfExistsField(query, purifier.purifyPlain(bio.ownerNickName), Urls.BIO_URL, "Bio", identity),
			SearchProvider.findIfExistsField(query, purifier.purifyPlain(bio.ownerDescription), Urls.BIO_URL, "Bio", identity))
		val metaResults = results
				.filter(_.isDefined)
				.map(_.get)
		val contentResults = SearchProvider.findIfExistsContent(query, contentCache, Urls.BIO_URL, "Bio", cutter)
		return metaResults.append(contentResults).toList
	}

	@Timed(name = "search.bio.preview.timer", absolute = true)
	override def searchPreview(queryRaw: String): List[SearchResult] = {
		val query = purifier.purifyPlain(queryRaw)
		val results = Stream(
			SearchProvider.findIfExistsField(query, purifier.purifyPlain(bio.ownerEmail), Urls.BIO_URL, "Bio", identity),
			SearchProvider.findIfExistsField(query, purifier.purifyPlain(bio.ownerName), Urls.BIO_URL, "Bio", identity),
			SearchProvider.findIfExistsField(query, purifier.purifyPlain(bio.ownerNickName), Urls.BIO_URL, "Bio", identity),
			SearchProvider.findIfExistsField(query, purifier.purifyPlain(bio.ownerDescription), Urls.BIO_URL, "Bio", identity))
		val metaResults = results
				.filter(_.isDefined)
				.map(_.get)
		val contentResults = SearchProvider.findIfExistsContent(query, contentCache, Urls.BIO_URL, "Bio", cutter)
		return metaResults.append(contentResults)
				.headOption
				.map(List(_))
				.getOrElse(List())
	}

	def reload(@Observes(notifyObserver = Reception.IF_EXISTS, during = TransactionPhase.AFTER_SUCCESS) e: BioUpdated): Unit = {
		load()
	}

	override def order(): Int = SearchProvider.BIO_SEARCH_ORDER
}
