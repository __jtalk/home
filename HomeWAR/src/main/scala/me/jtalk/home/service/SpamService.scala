/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service

import java.util
import javax.annotation.security.{RunAs, PermitAll, RolesAllowed}
import javax.ejb._
import javax.inject.Inject

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.configuration.Roles
import me.jtalk.home.model.SpamSettings
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption

import scala.collection.JavaConversions._
import scala.util.matching.Regex

@Singleton
@LocalBean
@RunAs(Roles.ADMINISTRATOR)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
class SpamService extends StrictLogging {

	@Inject
	private var ownerService: OwnerService = _

	private type CheckList = List[Regex]

	private var spamPatterns: CheckList = _
	private var excludePatterns: CheckList = _

	@PermitAll
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	def isSpam(data: String*): Boolean = {
		val spamList = getOrLoad("spam patterns", spamPatterns, (s, v) => s.spamPatterns = v, _.getSpamPatterns)
		val excludeList = getOrLoad("exclude patterns", excludePatterns, (s, v) => s.excludePatterns = v, _.getExcludedPatterns)
		val spam = data.map(countMatches(_, spamList)).sum
		val valid = data.map(countMatches(_, excludeList)).sum
		valid < spam
	}

	@PermitAll
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	def reload(): Unit = {
		spamPatterns = null
		excludePatterns = null
	}

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def loadSpamPatterns: List[String] = getOrLoad("spam patterns", spamPatterns, (s, v) => s.spamPatterns = v, _.getSpamPatterns)
			.map(_.toString)

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def loadExcludePatterns: List[String] = getOrLoad("exclude patterns", excludePatterns, (s, v) => s.excludePatterns = v, _.getExcludedPatterns)
			.map(_.toString)

	protected def countMatches(entry: String, patterns: CheckList) = patterns
			.map(_.findAllIn(entry).size)
			.sum

	protected def getOrLoad(patternType: String, existing: CheckList, setter: (SpamService, CheckList) => Unit, loader: SpamSettings => util.Set[String]): CheckList = {
		val loaded = Option(ownerService)
				.filterOr(_ => existing == null, _ => logger.debug("Spam service {} already initialized", patternType))
				.change(_ => logger.info("SpamService {} not initialized, performing lazy setup", patternType))
				.mapEnsure(_.load)
				.mapEnsure(_.getSpamSettings, _ => logger.error("Owner has no spam info"))
				.mapEnsure(loader.apply _)
				.map(_.map(p => p.r))
				.change(l => logger.info("SpanService has loaded {} rules for {}", l.size.toString, patternType))
				.map(_.toList)
		loaded.map(setter(this, _))
		loaded
				.orElse(Option(existing))
				.getOrElse(List())
	}
}
