/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service.search

import javax.annotation.ManagedBean
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

import org.apache.commons.configuration.Configuration
import org.apache.commons.lang3.StringUtils

@ManagedBean
@ApplicationScoped
class SearchPreviewCutter {

	private val SEARCH_LARGE_PREVIEW_CUT_LENGTH_PARAM = "me.jtalk.home.search.preview.large.length"
	private val SEARCH_LARGE_PREVIEW_CUT_LENGTH_DEFAULT = 500

	private val SEARCH_SMALL_PREVIEW_CUT_LENGTH_PARAM = "me.jtalk.home.search.preview.small.length"
	private val SEARCH_SMALL_PREVIEW_CUT_LENGTH_DEFAULT = 100

	@Inject
	private var config: Configuration = _

	def cutSmallPreview(content: String, index: Int): String = cutWithSize(content, index, smallPreviewCutSize())
	def cutLargePreview(content: String, index: Int): String = cutWithSize(content, index, largePreviewCutSize())

	protected def cutWithSize(content: String, index: Int, desiredSize: Int): String = {
		val start = 0 max (index - desiredSize / 2)
		StringUtils.abbreviate(content, start, desiredSize)
	}

	protected def smallPreviewCutSize() = config.getInt(SEARCH_SMALL_PREVIEW_CUT_LENGTH_PARAM, SEARCH_SMALL_PREVIEW_CUT_LENGTH_DEFAULT)
	protected def largePreviewCutSize() = config.getInt(SEARCH_LARGE_PREVIEW_CUT_LENGTH_PARAM, SEARCH_LARGE_PREVIEW_CUT_LENGTH_DEFAULT)
}
