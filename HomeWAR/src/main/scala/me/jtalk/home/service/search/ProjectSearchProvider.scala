/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service.search

import javax.annotation.{ManagedBean, PostConstruct}
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.event.{Observes, Reception, TransactionPhase}
import javax.inject.Inject

import com.codahale.metrics.annotation.{Timed, Metered}
import me.jtalk.home.faces.ProjectsHolderBean
import me.jtalk.home.faces.utils.{NavigationParameterUtils, Urls}
import me.jtalk.home.model.Project
import me.jtalk.home.service.event.ProjectUpdated
import me.jtalk.home.utils.SearchContentPurifier
import org.apache.commons.lang3.StringUtils

import scala.collection.JavaConversions._

@ManagedBean
@ApplicationScoped
class ProjectSearchProvider extends SearchProvider {

	@Inject
	private var projects: ProjectsHolderBean = _

	@Inject
	private var cutter: SearchPreviewCutter = _

	@Inject
	private var navigationParams: NavigationParameterUtils = _

	@Inject
	private var purifier: SearchContentPurifier = _

	private var contentsCache: List[ProjectDescription] = _

	@PostConstruct
	def load(): Unit = {
		contentsCache = projects.projects
				.filter(p => StringUtils.isNoneBlank(p.getDescription))
				.map(p => ProjectDescription(
					p.getName,
					p.getInternalName,
					purifier.purify(p.getDescription)))
				.toList
	}

	@Timed(name = "search.projects.timer", absolute = true)
	override def search(query: String): List[SearchResult] = {
		val pureQuery = purifier.purifyPlain(query)
		projects.projects.zip(contentsCache)
				.flatMap(t => searchProject(pureQuery, t._1).append(searchContent(pureQuery, t._2)))
				.toList
	}

	@Timed(name = "search.projects.preview.timer", absolute = true)
	override def searchPreview(query: String): List[SearchResult] = {
		val pureQuery = purifier.purifyPlain(query)
		projects.projects.zip(contentsCache)
				.map(t => searchProject(pureQuery, t._1).append(searchContent(pureQuery, t._2)).headOption)
		        .filter(_.isDefined)
		        .map(_.get)
		        .toList
	}

	protected def searchProject(query: String, meta: Project): Stream[SearchResult] = Stream(meta)
			.map(m => SearchProvider.findIfExistsField(
				query,
				purifier.purifyPlain(m.getName),
				formatUrl(m.getInternalName),
				"Project: " + m.getName,
				identity))
			.filter(_.isDefined)
			.map(_.get)

	protected def searchContent(query: String, data: ProjectDescription): Stream[SearchResult] = SearchProvider
			.findIfExistsContent(
				query,
				data.description,
				formatUrl(data.internalName),
				"Project: " + data.name,
				cutter
			)

	def reload(@Observes(notifyObserver = Reception.IF_EXISTS, during = TransactionPhase.AFTER_SUCCESS) e: ProjectUpdated): Unit = load()
	override def order(): Int = SearchProvider.PROJECTS_SEARCH_ORDER
	protected def formatUrl(internalName: String): String = s"${Urls.PROJECTS_URL}?${navigationParams.projectNameParameter}=${internalName}"

	private case class ProjectDescription(val name: String, val internalName: String, val description: String) {
	}

}
