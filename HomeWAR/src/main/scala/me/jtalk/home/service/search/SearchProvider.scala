/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service.search

import org.apache.commons.lang3.StringUtils

trait SearchProvider {
	def search(query: String): List[SearchResult]
	def searchPreview(query: String): List[SearchResult]
	def order(): Int
}

object SearchProvider {
	final var BLOG_SEARCH_ORDER = 80
	final var PROJECTS_SEARCH_ORDER = 50
	final val BIO_SEARCH_ORDER = 30

	def findIfExistsField(query: String, field: String, url: => String, name: => String, previewConverter: String => String): Option[SearchResult] = Option(field)
			.filter(_ => StringUtils.isNotBlank(query))
			.map(_.indexOf(query))
			.filter(_ >= 0)
			.map(_ => SearchResult(name, url, previewConverter(field), previewConverter(field)))

	def findIfExistsContent(query: String, content: String, url: => String, name: => String, previewCutter: SearchPreviewCutter): Stream[SearchResult]
	= Stream.iterate(-1)(i => content.indexOf(query, i + 1))
			.tail // Drop the first -1
			.takeWhile(_ >= 0)
			.map(i => SearchResult(name, url, previewCutter.cutSmallPreview(content, i), previewCutter.cutLargePreview(content, i)))
}