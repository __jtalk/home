/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service.search

import javax.annotation.{ManagedBean, PostConstruct}
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.Instance
import javax.inject.Inject

import com.codahale.metrics.annotation.{Timed, Metered}
import com.typesafe.scalalogging.StrictLogging

import scala.collection.JavaConversions._

@ManagedBean
@ApplicationScoped
class SearchService extends StrictLogging {

	@Inject
	var providersFound: Instance[SearchProvider] = _

	var providers: List[SearchProvider] = _

	@PostConstruct
	def prepare(): Unit = {
		providers = providersFound.toList.sortWith((a, b) => a.order > b.order)
		logger.info("Search service instantiated, {} providers found", providers.size.toString)
	}

	@Timed(name = "search.all.timer", absolute = true)
	def search(query: String): List[SearchResult] = providers.flatMap(_.search(query))

	@Timed(name = "search.all.preview.timer", absolute = true)
	def previewSearch(query: String): List[SearchResult] = providers.flatMap(_.searchPreview(query))
}
