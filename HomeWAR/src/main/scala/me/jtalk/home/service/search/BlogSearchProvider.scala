/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service.search

import javax.annotation.ManagedBean
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

import com.codahale.metrics.annotation.{Timed, Metered}
import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.faces.utils.{NavigationParameterUtils, Urls}
import me.jtalk.home.model.{Article, ArticleTag}
import me.jtalk.home.service.ArticleService
import me.jtalk.home.utils.SearchContentPurifier

import scala.collection.JavaConversions._

@ManagedBean
@ApplicationScoped
class BlogSearchProvider extends SearchProvider with StrictLogging {

	@Inject
	private var articleService: ArticleService = _

	@Inject
	private var navigationParams: NavigationParameterUtils = _

	@Inject
	private var previewCutter: SearchPreviewCutter = _

	@Inject
	private var purifier: SearchContentPurifier = _

	@Timed(name = "search.blog.timer", absolute = true)
	override def search(query: String): List[SearchResult] = {
		val pureQuery = purifier.purifyPlain(query)
		return articleService.search(pureQuery)
				.flatMap(searchArticle(pureQuery, _))
	}

	@Timed(name = "search.blog.preview.timer", absolute = true)
	override def searchPreview(query: String): List[SearchResult] = {
		val pureQuery = purifier.purifyPlain(query)
		return articleService.search(pureQuery)
				.map(searchArticle(pureQuery, _).headOption)
		        .filter(_.isDefined)
		        .map(_.get)
	}

	override def order(): Int = SearchProvider.BLOG_SEARCH_ORDER

	protected def searchArticle(query: String, article: Article): Stream[SearchResult] = {
		logger.debug("Searching article {} for query {}", article.getInternalTitle, query)
		val postResults = searchPost(query, article)
		val tagsResults = searchTags(query, article)
		val contentResults = searchPostContent(query, article)
		return tagsResults append postResults append contentResults
	}

	protected def searchPostContent(query: String, article: Article): Stream[SearchResult]
	= SearchProvider.findIfExistsContent(
		query,
		article.getPurifiedContent,
		formatUrl(article.getInternalTitle),
		"Blog: " + article.getTitle,
		previewCutter)

	protected def searchPost(query: String, article: Article): Stream[SearchResult] = Stream(article)
			.map(a => SearchProvider.findIfExistsField(
				query,
				purifier.purifyPlain(article.getTitle),
				formatUrl(a.getInternalTitle),
				"Blog: " + a.getTitle,
				identity))
			.filter(_.isDefined)
			.map(_.get)

	protected def searchTags(query: String, article: Article): Stream[SearchResult] = article.getTags.toStream
			.map(t => SearchProvider.findIfExistsField(
				query,
				purifier.purifyPlain(t.getName),
				formatUrl(article.getInternalTitle),
				"Blog tag: " + article.getTitle,
				_ => ""))
			.filter(_.isDefined)
			.map(_.get)

	protected def formatUrl(internalTitle: String): String = s"${Urls.POST_URL}?${navigationParams.blogPostNameParameter}=${internalTitle}"
}
