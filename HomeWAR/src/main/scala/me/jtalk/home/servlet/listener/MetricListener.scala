/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.servlet.listener

import javax.inject.Inject
import javax.servlet.annotation.WebListener

import com.codahale.metrics.MetricRegistry
import com.codahale.metrics.servlets.MetricsServlet.ContextListener

@WebListener
class MetricListener extends ContextListener {

	@Inject
	private var registry: MetricRegistry = _

	override def getMetricRegistry: MetricRegistry = registry
}
