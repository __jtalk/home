/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.servlet

import javax.inject.Inject
import javax.servlet.annotation.WebServlet
import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}
import javax.ws.rs.core.MediaType

import com.rometools.rome.io.SyndFeedOutput
import me.jtalk.home.faces.utils.NavigationParameterUtils
import me.jtalk.home.faces.utils.rss.RssUtils
import me.jtalk.home.utils.rss.FeedType
import org.apache.commons.lang3.BooleanUtils

@WebServlet(Array("/rss"))
class RssServlet extends HttpServlet {

	@Inject
	private var rss: RssUtils = _

	@Inject
	private var navigationParams: NavigationParameterUtils = _

	override def doGet(req: HttpServletRequest, resp: HttpServletResponse): Unit = {
		val urlPrefix = s"${req.getServerName}:${req.getServerPort}${req.getContextPath}"
		val feedType = loadFeedType(req)
    val showFull = Option(req.getParameter(navigationParams.showFullArticleRssParameter))
                        .exists(BooleanUtils.toBoolean)
    val feed = rss.makeFeed(feedType, !showFull, local => s"${urlPrefix}${local}")
    resp.setContentType(resolveContentType(feedType))
		resp.setStatus(HttpServletResponse.SC_OK)
		new SyndFeedOutput().output(feed, resp.getWriter)
	}

	private def loadFeedType(req: HttpServletRequest): FeedType = Option(req.getParameter(navigationParams.rssFeedTypeParameter))
																																		.flatMap(feedCode => Option(FeedType.fromCode(feedCode).orElse(null)))
																																		.getOrElse(FeedType.RSS_20)

  private def resolveContentType(feedType: FeedType): String = if (feedType.isAtom) {
    MediaType.APPLICATION_ATOM_XML
  } else {
    "application/rss+xml"
  }
}
