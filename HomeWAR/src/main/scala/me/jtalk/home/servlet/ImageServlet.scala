/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.servlet

import javax.inject.Inject
import javax.servlet.ServletConfig
import javax.servlet.annotation.WebServlet
import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}
import javax.ws.rs.core.Response.Status

import com.google.common.net.MediaType
import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.faces.utils.DefaultPhotoUtils
import me.jtalk.home.service.{UploadedImageService, OwnerService}
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption
import org.apache.commons.io.IOUtils

import scala.beans.BeanProperty

/**
  * Type is fixed to PNG to ease images handling and avoid storing image type into the database.
  */
@WebServlet(Array("/image/dynamic/*"))
class ImageServlet extends HttpServlet with StrictLogging {

	@Inject
	private var uploadedImages: UploadedImageService = _

	override def init(config: ServletConfig) = {
		super.init(config)
		logger.info("Image servlet instantiated")
	}

	override def doGet(req: HttpServletRequest, resp: HttpServletResponse) {
		resolveImage(req) match {
			case Some(image) => {
				resp.setContentType(MediaType.PNG.toString)
				resp.setStatus(Status.OK.getStatusCode)
				IOUtils.write(image, resp.getOutputStream)
				logger.debug(s"Image found for ${req.getRequestURI}")
			}
			case None => {
				resp.setStatus(Status.NOT_FOUND.getStatusCode)
				logger.debug(s"Image not found for ${req.getRequestURI}")
			}
		}
	}

	protected def resolveImage(req: HttpServletRequest): Option[Array[Byte]] = {
		val fileName = req.getPathInfo.substring(1) // Skip first slash
		getUploadedImage(fileName.substring(0, fileName.length - ".png".length))
	}

	protected def getUploadedImage(id: String): Option[Array[Byte]] = uploadedImages.findContentReadOnly(id)
			.mapEnsure(_.getContent)
}