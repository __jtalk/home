/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import java.io.InputStream
import javax.annotation.{ManagedBean, PostConstruct}
import javax.enterprise.event.Event
import javax.inject.{Inject, Named}
import javax.servlet.http.Part
import javax.transaction.Transactional

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.configuration.Constants
import me.jtalk.home.faces.utils.{BioUtils, ErrorUtils}
import me.jtalk.home.model.Owner
import me.jtalk.home.service.OwnerService
import me.jtalk.home.service.event.BioUpdated
import me.jtalk.home.utils.ImageUtils
import org.apache.commons.configuration.Configuration
import org.omnifaces.cdi.ViewScoped

import scala.beans.BeanProperty

@Named
@ManagedBean
@ViewScoped
class BioEditBean extends Serializable with StrictLogging {

	val MAX_PHOTO_SIZE_PARAM = "me.jtalk.home.owner.photo.size.max"
	val SCALED_PHOTO_WIDTH_PX = 200

	@Inject
	private var bioUtils: BioUtils = _

	@Inject
	private var ownerService: OwnerService = _

	@Inject
	private var config: Configuration = _

	@Inject
	private var errorUtils: ErrorUtils = _

	@Inject
	private var onChange: Event[BioUpdated] = _

	@BeanProperty
	var ownerName: String = _

	@BeanProperty
	var ownerNickname: String = _

	@BeanProperty
	var ownerEmail: String = _

	@BeanProperty
	var ownerDescription: String = _

	@BeanProperty
	var bio: String = _

	@BeanProperty
	var photoPart: Part = _

	@PostConstruct
	def init() = {
		ownerName = bioUtils.ownerName
		ownerNickname = bioUtils.ownerNickName
		ownerEmail = bioUtils.ownerEmail
		ownerDescription = bioUtils.ownerDescription
		bio = bioUtils.contentRaw
	}

	@Transactional
	def save(): Unit = {
		val owner = ownerService.load();
		updateEntries(owner)
		updatePhoto(owner)
		onChange.fire(new BioUpdated)
	}

	def maxPhotoSize(): Long = Constants.MAX_PHOTO_SIZE max config.getInt(MAX_PHOTO_SIZE_PARAM, 0)

	protected def updateEntries(owner: Owner) {
		owner.getEntries.put(Constants.OWNER_USER_NAME_KEY, ownerName)
		owner.getEntries.put(Constants.OWNER_USER_NICKNAME_KEY, ownerNickname)
		owner.getEntries.put(Constants.OWNER_USER_EMAIL_KEY, ownerEmail)
		owner.getEntries.put(Constants.OWNER_USER_DESCRIPTION_KEY, ownerDescription)
		owner.getEntries.put(Constants.OWNER_USER_BIO_KEY, bio)
	}

	protected def updatePhoto(owner: Owner) {
		Option(photoPart)
		        .flatMap(p => tryReadPhoto(p.getInputStream, p.getContentType))
		        .map(owner.setPhoto _)
	}

	protected def tryReadPhoto(in: InputStream, mime: String): Option[Array[Byte]] = ImageUtils.toImage(in, mime, resizeWidth = SCALED_PHOTO_WIDTH_PX)
			.flatMap(ImageUtils.toByteArrayOf(_, "png"))
	        .map(Some(_))
	        .get
}
