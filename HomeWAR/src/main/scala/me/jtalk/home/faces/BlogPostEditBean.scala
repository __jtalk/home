/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import java.time._
import java.time.format.DateTimeFormatter
import java.util.{ArrayList => JArrayList}
import javax.annotation.{ManagedBean, PostConstruct, PreDestroy}
import javax.enterprise.event.Event
import javax.faces.application.FacesMessage
import javax.faces.component.UIInput
import javax.faces.context.FacesContext
import javax.inject.{Inject, Named}
import javax.servlet.http.HttpServletRequest

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.exception.NotFoundException
import me.jtalk.home.faces.utils.{ErrorUtils, NavigationParameterUtils, RenderingUtils}
import me.jtalk.home.service.BlogPostEdit
import me.jtalk.home.service.event.ArticlesUpdated
import me.jtalk.home.utils.SearchContentPurifier
import me.jtalk.home.utils.scala.EnhancedBoolean.enhanceBoolean
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption
import org.omnifaces.cdi.ViewScoped

import scala.beans.BeanProperty
import scala.util.{Failure, Try}

@Named
@ManagedBean
@ViewScoped
class BlogPostEditBean extends Serializable with StrictLogging {

	private val dateFormatter = DateTimeFormatter.ofPattern("MM/dd/yyyy")
	private val timeFormatter12 = DateTimeFormatter.ofPattern("hh:mm:ss a")
	private val timeFormatter24 = DateTimeFormatter.ofPattern("HH:mm:ss")

	@Inject
	private var editor: BlogPostEdit = _

	@Inject
	private var navigationParams: NavigationParameterUtils = _

	@Inject
	private var errors: ErrorUtils = _

	@Inject
	private var purifier: SearchContentPurifier = _

	@Inject
	private var renderer: RenderingUtils = _

	@Inject
	private var postChanged: Event[ArticlesUpdated] = _

	@BeanProperty
	var creationTime: String = _

	@BeanProperty
	var creationDate: String = _

	@BeanProperty
	var shortNameInput: UIInput = _

	@BeanProperty
	var creationTimeInput: UIInput = _

	@BeanProperty
	var creationDateInput: UIInput = _

	@PostConstruct
	def load(): Unit = {
		Option(FacesContext.getCurrentInstance.getExternalContext.getRequest.asInstanceOf[HttpServletRequest])
				.mapEnsure(_.getParameter(navigationParams.blogPostNameParameter), _ => logger.warn("No article name provided to the editor loading routine"))
				.map(editor.loadByName _)
				.flatMap(_.toOption)
				.getOrElse(throw new NotFoundException("No article found"))
		initCreationDateTime()
	}

	def get() = editor.get
	def getTags() = editor.getTags
	def setTags(t: JArrayList[String]): Unit = editor.setTags(t)
	def getContent() = editor.get.getContent
	def setContent(content: String) = editor.get().setContent(content, prepareSearchableContent(content))

	def addTag(name: String): Unit = logger.error("Tag added: {}", name)

	def clear(): Unit = {
		val id = editor.get.getId
		editor.reset()
		editor.load(id)
		initCreationDateTime()
	}

	def save(): Unit = {
		if (tryApplyCreationDateTime()) {
			editor.save()
					.toOption
					.changeFalse(errors.reportValidationError(shortNameInput, FacesMessage.SEVERITY_ERROR, "Duplicate name", "This internal name already exists"))
					.change(_ => postChanged.fire(new ArticlesUpdated))
		}
	}

	@PreDestroy
	def onDestroy() = editor.remove()

	protected def prepareSearchableContent(content: String): String = {
		val rendered = renderer.render(content)
		purifier.purify(rendered)
	}

	protected def initCreationDateTime(): Unit = {
		val created = editor.get.getCreated.withZoneSameInstant(ZoneId.of("UTC"))
		creationDate = created.toLocalDate.format(dateFormatter)
		creationTime = created.toLocalTime.format(timeFormatter12)
	}

	protected def tryApplyCreationDateTime(): Boolean = {
		val dateResult = Try(dateFormatter.parse(creationDate))
				.recoverWith({
					case e: Exception => {
						logger.debug("Error parsing date pattern {}", creationDate)
						Failure(e)
					}
				})
				.toOption
				.map(LocalDate.from _)
				.changeFalse(errors.reportValidationError(creationDateInput, FacesMessage.SEVERITY_ERROR, "Creation date invalid", "Invalid creation date value"))
		val timeResult = Try(timeFormatter12.parse(creationTime))
				.recoverWith({
					case e: Exception => {
						logger.debug("Error parsing 12-hour time pattern {}, trying 24-hour", creationTime)
						Failure(e)
					}
				})
				.orElse(Try(timeFormatter24.parse(creationTime)))
				.recoverWith({
					case e: Exception => {
						logger.debug("Error parsing 24-hour time pattern {}", creationTime)
						Failure(e)
					}
				})
				.toOption
				.map(LocalTime.from _)
				.changeFalse(errors.reportValidationError(creationTimeInput, FacesMessage.SEVERITY_ERROR, "Creation time invalid", "Invalid creation time value"))
		dateResult.flatMap(date => {
			timeResult.map(time => {
				val local = LocalDateTime.of(date, time)
				val zoned = ZonedDateTime.ofLocal(local, ZoneId.of("UTC"), null)
				editor.get.setCreated(zoned)
			})
		})
				.exists(_ => true)
	}
}
