/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces.utils

import java.util.{ArrayList => JArrayList}
import javax.annotation.{PostConstruct, ManagedBean}
import javax.enterprise.context.ApplicationScoped
import javax.inject.{Inject, Named}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.model.{ArticleComment, Article, ArticleTag}
import me.jtalk.home.service.ArticleService
import me.jtalk.home.utils.scala.EnhancedOption._
import org.apache.commons.lang3.StringUtils

@Named
@ManagedBean
@ApplicationScoped
class BlogUtils extends Serializable with StrictLogging {

	val previewSeparator = "&cut&"

	@Inject
	private var renderingUtils: RenderingUtils = _

	def previewContent(a: Article): String = Option(a)
			.mapEnsure(_.getContent, a => logger.warn("Article with no content preview requested, id {}, name {}", a.getId, a.getInternalTitle))
			.map(s => StringUtils.splitByWholeSeparator(s, previewSeparator))
			.map(p => p(0))
			.orElse(Option(a.getContent))
			.map(renderingUtils.render _)
			.get

	def fullContent(a: Article): String = Option(a)
	        .mapEnsure(_.getContent, a => logger.warn("Article with no content is empty, id {}, name {}", a.getId, a.getInternalTitle))
	        .mapEnsure(StringUtils.replaceOnce(_, previewSeparator, ""))
	        .map(renderingUtils.render _)
	        .getOrElse("")

	def tags(a: Article): JArrayList[ArticleTag] = Option(a)
			.mapEnsure(_.getTags)
			.map(new JArrayList[ArticleTag](_))
			.getOrElse(new JArrayList[ArticleTag]())

	def comments(a: Article): JArrayList[ArticleComment] = Option(a)
	        .mapEnsure(_.getComments)
			.map(new JArrayList(_))
	        .getOrElse(new JArrayList())
}
