/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces.utils

import javax.annotation.ManagedBean
import javax.enterprise.context.SessionScoped
import javax.inject.{Inject, Named}

import cn.apiclub.captcha.Captcha
import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.utils.ImageUtils

@Named
@ManagedBean
@SessionScoped
class CaptchaHolder extends Serializable with StrictLogging {

	@Inject
	private var captchaSource: CaptchaUtils = _

	private var captcha: Option[Captcha] = None
	private var approved = false

	def create(): Unit = {
		captcha = Some(captchaSource.create())
		logger.debug("Creating new captcha")
	}

	def approve(): Unit = {
		approved = true
	}

	def clear(): Unit = {
		captcha = None
		logger.debug("Removing captcha from holder")
	}

	def image(): Option[Array[Byte]] = captcha
	        .map(_.getImage)
	        .map(ImageUtils.toByteArrayOf(_, "png"))
	        .map(_.get)

	def check(value: String): Boolean = captcha
	        .map(_.isCorrect(value))
	        .getOrElse(true)

	def isRequired(): Boolean = captcha.isDefined
	def isApproved() = approved
}
