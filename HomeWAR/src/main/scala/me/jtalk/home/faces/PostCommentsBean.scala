/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import javax.annotation.{PostConstruct, ManagedBean}
import javax.faces.application.FacesMessage
import javax.inject.{Inject, Named}
import javax.ws.rs.BeanParam

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.configuration.Constants
import me.jtalk.home.faces.utils.{ErrorUtils, CaptchaHolder, CaptchaUtils}
import me.jtalk.home.service.{SpamService, ArticleService}
import me.jtalk.home.utils.scala.EnhancedBoolean.enhanceBoolean
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption
import org.apache.commons.lang3.StringUtils
import org.omnifaces.cdi.ViewScoped

import scala.beans.BeanProperty

@Named
@ManagedBean
@ViewScoped
class PostCommentsBean extends Serializable with StrictLogging {

	@Inject
	private var articleService: ArticleService = _

	@Inject
	private var postViewBean: PostViewBean = _

	@Inject
	private var spamService: SpamService = _

	@Inject
	private var user: CurrentUser = _

	@Inject
	private var captcha: CaptchaHolder = _

	@Inject
	private var errors: ErrorUtils = _

	@BeanProperty
	var author: String = _

	@BeanProperty
	var hiddenField: String = _

	@BeanProperty
	var content: String = _

	def addComment(): Unit = {
		if (StringUtils.isNotEmpty(hiddenField)) {
			logger.error("Bot is trying to leave a comment: name '{}', hidden '{}', content '{}'", author, hiddenField, content)
			return
		}
		if (!captcha.isApproved && !user.isAdmin() && spamService.isSpam(author, content)) {
			// Administrator is allowed to post whatever he wants
			// Captch value is non-null whenever captcha rendering was requested. CaptchaValidator takes care of it's actual check
			// If captcha is null, then bot check will be performed, otherwise we assume user has proven his human origin :)
			logger.warn("Spam service thinks bot is trying to invade us with name '{}' and text '{}'", author, content)
			captcha.create()
			errors.reportGlobalValidationError(FacesMessage.SEVERITY_ERROR, "Captcha needed", "Your message looks suspicious, please, confirm your human origin")
			return
		}
		val postId = postViewBean.article.getId
		author = Option(author)
				.filter(!_.isEmpty)
				.getOrElse("<anonymous>")
		articleService.addComment(postId, author, content)
		author = ""
		content = ""
		postViewBean.reload()
	}

	def markMine(id: String): Unit = articleService.setCommentOwnerMark(id, true)
			.toOption
			.change(_ => postViewBean.reload())

	def unmarkMine(id: String): Unit = articleService.setCommentOwnerMark(id, false)
			.toOption
			.change(_ => postViewBean.reload())

	def remove(id: String): Unit = articleService.removeComment(id)
			.toOption
			.map(_ => postViewBean.reload())

	def getMaxAuthorLength(): Int = Constants.MAX_COMMENT_AUTHOR_NAME_SIZE
	def getMaxContentLength(): Int = Constants.MAX_COMMENT_SIZE
}
