/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import javax.annotation.ManagedBean
import javax.enterprise.context.RequestScoped
import javax.faces.application.FacesMessage
import javax.inject.{Inject, Named}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.configuration.Constants
import me.jtalk.home.faces.utils.{ErrorUtils, ImageLoader}
import me.jtalk.home.service.UploadedImageService
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption

import scala.beans.BeanProperty

@Named
@ManagedBean
@RequestScoped
class FileUploadBean extends StrictLogging {

	@Inject
	private var uploadedImages: UploadedImageService = _

	@Inject
	private var errors: ErrorUtils = _

	@Inject
	private var imagesList: FilesListBean = _

	@BeanProperty
	var description: String = _

	@BeanProperty
	val file: ImageLoader = new ImageLoader()

	def upload(): Unit = file.get()
			.changeFalse(logger.warn("File uploading requested with no file provided"))
			.changeFalse(errors.reportGlobalValidationError(FacesMessage.SEVERITY_ERROR, "No image data", "No image date has been provided for upload"))
			.map(uploadedImages.add(description, _))
			.change(logger.debug("Image {} has been created", _))
	        .change(_ => imagesList.load())

	def getMaxDescriptionSize() = Constants.MAX_UPLOADED_IMAGE_DESCRIPTION
	def getMaxFileSize() = Constants.MAX_UPLOADED_IMAGE_SIZE
}
