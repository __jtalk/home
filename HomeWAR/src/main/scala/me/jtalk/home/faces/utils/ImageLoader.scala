/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces.utils

import java.io.InputStream
import javax.servlet.http.Part

import me.jtalk.home.utils.ImageUtils

import scala.beans.BeanProperty

class ImageLoader(outputType: String = "png") {

	@BeanProperty
	var part: Part = _

	def get(): Option[Array[Byte]] = Option(part)
			.flatMap(p => tryReadPhoto(p.getInputStream, p.getContentType))
	def getWithWidth(width: Int) = Option(part)
			.flatMap(p => tryReadPhoto(p.getInputStream, p.getContentType, newWidth = width))
	def getWithHeight(height: Int) = Option(part)
			.flatMap(p => tryReadPhoto(p.getInputStream, p.getContentType, newHeight = height))

	protected def tryReadPhoto(in: InputStream, mime: String, newWidth: Int = -1, newHeight: Int = -1): Option[Array[Byte]]
	= ImageUtils.toImage(in, mime, resizeWidth = newWidth, resizeHeight = newHeight)
			.flatMap(ImageUtils.toByteArrayOf(_, outputType))
			.map(Some(_))
			.get
}
