/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import java.util.{ArrayList => JArrayList}
import javax.annotation.{ManagedBean, PostConstruct}
import javax.inject.{Inject, Named}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.faces.utils.PaginationUtils
import me.jtalk.home.model.UploadedImage
import me.jtalk.home.service.UploadedImageService
import me.jtalk.home.utils.scala.EnhancedBoolean.enhanceBoolean
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption
import org.omnifaces.cdi.ViewScoped

import scala.beans.BeanProperty
import scala.collection.JavaConversions._
import scala.math.ceil

@Named
@ManagedBean
@ViewScoped
class FilesListBean extends Serializable with StrictLogging {

	@Inject
	private var pagination: PaginationUtils = _

	@Inject
	private var uploadedImages: UploadedImageService = _

	@BeanProperty
	var imagesPage: JArrayList[UploadedImage] = _

	@BeanProperty
	var pagesCount: Long = _

	@PostConstruct
	def load(): Unit = {
		val loaded = uploadedImages.findMetadataPageReadOnly(pagination.current, pagination.getBlogPageSize)
		imagesPage = new JArrayList(loaded)
		pagesCount = 1L max ceil(uploadedImages.count().toDouble / pagination.getImagesPageSize().toDouble).toLong
	}

	def delete(id: String): Unit = uploadedImages.remove(id)
			.toOption
			.flatMap(_ => imagesPage.find(_.getId == id))
			.change(imagesPage.remove _)
}
