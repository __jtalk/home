/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces.utils

import javax.annotation.{PostConstruct, ManagedBean}
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.event.{TransactionPhase, Observes}
import javax.inject.{Inject, Named}
import java.util.{ArrayList => JArrayList}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.model.Article
import me.jtalk.home.service.ArticleService
import me.jtalk.home.service.event.ArticlesUpdated
import org.apache.commons.configuration.Configuration

@Named
@ManagedBean
@ApplicationScoped
class LatestPostsUtils extends StrictLogging {

	val LATEST_POSTS_SIZE_PARAM = "me.jtalk.home.blog.posts.latest.size"
	val LATEST_POSTS_SIZE_DEFAULT = 5

	@Inject
	private var articleService: ArticleService = _

	@Inject
	private var config: Configuration = _

	private var latest: JArrayList[Article] = _

	@PostConstruct
	def load(): Unit = {
		val size = postsSize()
		val loaded = articleService.loadPageReadOnly(0, size)
		latest = new JArrayList(loaded)
		logger.info("Latest posts list updated, {} expected, {} loaded", size.toString, loaded.size.toString)
	}

	def getPosts() = latest
	def reload(@Observes(during = TransactionPhase.AFTER_SUCCESS) e: ArticlesUpdated) = load()
	protected def postsSize() = config.getInt(LATEST_POSTS_SIZE_PARAM, LATEST_POSTS_SIZE_DEFAULT)
}
