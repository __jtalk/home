/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces.utils

import javax.annotation.{ManagedBean, PostConstruct}
import javax.enterprise.context.RequestScoped
import javax.faces.context.FacesContext
import javax.inject.{Inject, Named}
import javax.servlet.http.HttpServletRequest

import org.apache.commons.configuration.Configuration
import me.jtalk.home.utils.scala.EnhancedOption._

import scala.beans.BeanProperty

@Named
@ManagedBean
@RequestScoped
class PaginationUtils extends Serializable {

	private val BLOG_PAGE_SIZE_PARAM = "me.jtalk.home.blog.page.size"
	private val BLOG_PAGE_SIZE_DEFAULT = 10

	private val IMAGES_PAGE_SIZE_PARAM = "me.jtalk.home.admin.images.page.size"
	private val IMAGES_PAGE_SIZE_DEFAULT = 40

	private val SEARCH_PAGE_SIZE_PARAM = "me.jtalk.home.admin.search.page.size"
	private val SEARCH_PAGE_SIZE_DEFAULT = 40

	@Inject
	private var config: Configuration = _

	@BeanProperty
	var current: Int = 0

	@BeanProperty
	val pageParameter = "page"

	@PostConstruct
	def load(): Unit = {
		current = Option(FacesContext.getCurrentInstance.getExternalContext.getRequest.asInstanceOf[HttpServletRequest])
				.mapEnsure(_.getParameter(pageParameter))
				.map(_.toInt)
				.getOrElse(0)
	}

	def isCurrent(index: Int): Boolean = current == index
	def getBlogPageSize(): Int = config.getInt(BLOG_PAGE_SIZE_PARAM, BLOG_PAGE_SIZE_DEFAULT)
	def getImagesPageSize(): Int = config.getInt(IMAGES_PAGE_SIZE_PARAM, IMAGES_PAGE_SIZE_DEFAULT)
	def getSearchPageSize(): Int = config.getInt(SEARCH_PAGE_SIZE_PARAM, SEARCH_PAGE_SIZE_DEFAULT)
}
