/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import java.util.{ArrayList => JArrayList}
import javax.annotation.{ManagedBean, PostConstruct}
import javax.faces.context.FacesContext
import javax.inject.{Inject, Named}
import javax.servlet.http.HttpServletRequest

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.faces.utils.{NavigationParameterUtils, PaginationUtils}
import me.jtalk.home.service.search.{SearchResult, SearchService}
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption
import me.jtalk.home.utils.scala.EnhancedSeq.enhanceGenTraversable
import org.omnifaces.cdi.ViewScoped

import scala.beans.BeanProperty
import scala.math

@Named
@ManagedBean
@ViewScoped
class SearchBean extends Serializable with StrictLogging {

	@Inject
	private var searchService: SearchService = _

	@Inject
	private var pagination: PaginationUtils = _

	@Inject
	private var navigationParams: NavigationParameterUtils = _

	@BeanProperty
	var query: String = _

	private var results: List[SearchResult] = _

	@PostConstruct
	def load(): Unit = {
		val queryParam = Option(FacesContext.getCurrentInstance.getExternalContext.getRequest.asInstanceOf[HttpServletRequest])
				.mapEnsure(_.getParameter(navigationParams.searchQueryNameParameter))
		query = queryParam.getOrElse("")
		results = queryParam
				.map(searchService.search _)
				.getOrElse(List())
	}

	def getPage(): JArrayList[SearchResult] = results.toStream
			.drop(pagination.current * pagination.getSearchPageSize())
			.take(pagination.getSearchPageSize())
			.toJavaList[JArrayList[SearchResult]](new JArrayList)

	def getPagesCount(): Long = 1L max math.ceil(results.size.toDouble / pagination.getSearchPageSize().toDouble).toLong
}
