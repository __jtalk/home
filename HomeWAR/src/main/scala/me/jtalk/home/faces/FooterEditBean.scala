/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import java.util.{ArrayList => JArrayList}
import javax.annotation.{ManagedBean, PostConstruct}
import javax.enterprise.event.Event
import javax.faces.application.FacesMessage
import javax.inject.{Inject, Named}
import javax.transaction.Transactional

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.faces.utils.ErrorUtils
import me.jtalk.home.model.FooterLink
import me.jtalk.home.service.OwnerService
import me.jtalk.home.service.event.FooterUpdated
import org.omnifaces.cdi.ViewScoped

import scala.beans.BeanProperty

@Named
@ManagedBean
@ViewScoped
class FooterEditBean extends Serializable with StrictLogging {

	@Inject
	private var owner: OwnerService = _

	@Inject
	private var errors: ErrorUtils = _

	@Inject
	private var onChange: Event[FooterUpdated] = _

	@BeanProperty
	var links: JArrayList[FooterLink] = _

	@PostConstruct
	def load(): Unit = {
		links = new JArrayList(owner.loadReadOnly().getFooterLinks)
	}

	@Transactional
	def create(title: String, url: String): Unit = {
		val link = new FooterLink(title, url)
		val list = owner.load().getFooterLinks
		if (list.contains(link)) {
			errors.reportGlobalValidationError(FacesMessage.SEVERITY_ERROR, "Duplicating link", "This link already exists")
		} else {
			list.add(link)
			links = new JArrayList(list)
			onChange.fire(new FooterUpdated)
		}
	}

	@Transactional
	def moveUp(link: FooterLink): Unit = {
		val list = owner.load().getFooterLinks
		val index = list.indexOf(link)
		if (index <= 0) {
			logger.error("Link {} is requested to be moved up, but it's index is {}", link, index.toString)
		} else {
			list.remove(index)
			list.add(index - 1, link)
			links = new JArrayList(list)
			onChange.fire(new FooterUpdated)
		}
	}

	@Transactional
	def moveDown(link: FooterLink): Unit = {
		val list = owner.load().getFooterLinks
		val index = list.indexOf(link)
		if (index < 0 || index >= list.size) {
			logger.error("Link {} is requested to be moved down, but it's index is {}", link, index.toString)
		} else {
			list.remove(index)
			list.add(index + 1, link)
			links = new JArrayList(list)
			onChange.fire(new FooterUpdated)
		}
	}

	@Transactional
	def delete(link: FooterLink): Unit = {
		val list = owner.load().getFooterLinks
		if (list.contains(link)) {
			list.remove(link)
			links = new JArrayList(list)
			onChange.fire(new FooterUpdated)
		} else {
			logger.error("Link {} is requested to be removed, but it's not found", link)
		}
	}
}
