/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.jtalk.home.faces.utils.error

import java.lang.reflect.InvocationTargetException
import java.util
import java.util.Collections
import javax.el.ELException
import javax.faces.FacesException
import javax.faces.application.ProjectStage
import javax.faces.component.UIComponent
import javax.faces.context.{ExceptionHandler, FacesContext}
import javax.faces.el.EvaluationException
import javax.faces.event.{AbortProcessingException, ExceptionQueuedEvent, ExceptionQueuedEventContext, SystemEvent}

import com.typesafe.scalalogging.StrictLogging

/**
  * We need this because Wildfly's JSF implementation is out of sync: JSF itself throws deprecated
  * EvaluationExceptions while handler only unwraps ELExceptions. We need to fix this.
  *
  * This code is based on ExceptionHandlerImpl from the refrerence JSF implementation.
  *
  */
class ELAwareExceptionHandler extends ExceptionHandler with StrictLogging {

	private var unhandledExceptions: util.LinkedList[ExceptionQueuedEvent] = null
	private var handledExceptions: util.LinkedList[ExceptionQueuedEvent] = null
	private var handled: ExceptionQueuedEvent = null

	override def getHandledExceptionQueuedEvent: ExceptionQueuedEvent = handled

	override def handle(): Unit = {
		val i: util.Iterator[ExceptionQueuedEvent] = getUnhandledExceptionQueuedEvents.iterator
		while (i.hasNext) {
			val event: ExceptionQueuedEvent = i.next
			val context: ExceptionQueuedEventContext = event.getSource.asInstanceOf[ExceptionQueuedEventContext]
			try {
				val t: Throwable = context.getException
				if (isRethrown(t)) {
					handled = event
					val unwrapped: Throwable = getRootCause(t)
					if (unwrapped != null) {
						throwIt(context.getContext, new FacesException(unwrapped.getMessage, unwrapped))
					}
					else {
						if (t.isInstanceOf[FacesException]) {
							throwIt(context.getContext, t.asInstanceOf[FacesException])
						}
						else {
							throwIt(context.getContext, new FacesException(t.getMessage, t))
						}
					}
				}
				else {
					log(context)
				}
			} finally {
				if (handledExceptions == null) {
					handledExceptions = new util.LinkedList[ExceptionQueuedEvent]
				}
				handledExceptions.add(event)
				i.remove()
			}
		}
	}

	override def isListenerForSource(source: AnyRef): Boolean = source.isInstanceOf[ExceptionQueuedEventContext]

	override def processEvent(event: SystemEvent) {
		if (event != null) {
			if (unhandledExceptions == null) {
				unhandledExceptions = new util.LinkedList[ExceptionQueuedEvent]
			}
			unhandledExceptions.add(event.asInstanceOf[ExceptionQueuedEvent])
		}
	}

	override def getRootCause(t: Throwable): Throwable = Stream.iterate(t)(_.getCause)
			.takeWhile(_ != null)
			.dropWhile(e => shouldUnwrap(e.getClass))
			.headOption
	        .orNull

	override def getUnhandledExceptionQueuedEvents: util.List[ExceptionQueuedEvent] = Option(unhandledExceptions)
			.getOrElse(Collections.emptyList())

	override def getHandledExceptionQueuedEvents: util.List[ExceptionQueuedEvent] = Option(handledExceptions)
			.getOrElse(Collections.emptyList[ExceptionQueuedEvent])

	private def throwIt(ctx: FacesContext, fe: FacesException) {
		val isDevelopment: Boolean = ctx.isProjectStage(ProjectStage.Development)
		if (isDevelopment) {
			ctx.getExternalContext.getRequestMap.put("com.sun.faces.error.view", ctx.getViewRoot)
		}
		throw fe
	}

	private def shouldUnwrap(c: Class[_ <: Throwable]): Boolean = {
		classOf[FacesException] == c ||
				c.getName == "org.jboss.weld.exceptions.WeldException" || // Not in classpath, implementation-specific
				classOf[InvocationTargetException] == c ||
				classOf[ELException] == c ||
				classOf[EvaluationException] == c
	}

	private def isRethrown(t: Throwable): Boolean = !t.isInstanceOf[AbortProcessingException]

	private def log(exceptionContext: ExceptionQueuedEventContext) {
		val c: UIComponent = exceptionContext.getComponent
		val t: Throwable = exceptionContext.getException
		logger.error("Error handled from view", t)
	}
}
