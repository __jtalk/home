/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import java.util.Collections
import javax.annotation.{ManagedBean, PostConstruct, PreDestroy}
import javax.ejb.EJB
import javax.enterprise.event.Event
import javax.faces.context.FacesContext
import javax.inject.{Inject, Named}
import javax.servlet.http.HttpServletRequest

import com.typesafe.scalalogging.LazyLogging
import me.jtalk.home.configuration.Constants
import me.jtalk.home.faces.utils.ImageLoader
import me.jtalk.home.model.ProjectLink
import me.jtalk.home.service.event.ProjectUpdated
import me.jtalk.home.service.{ProjectEdit, ProjectService}
import me.jtalk.home.utils.scala.EnhancedOption._
import me.jtalk.home.utils.scala.EnhancedBoolean._
import me.jtalk.home.utils.scala.EnhancedAny._
import org.apache.commons.configuration.Configuration
import org.omnifaces.cdi.ViewScoped

import scala.beans.BeanProperty

@Named
@ManagedBean
@ViewScoped
class ProjectEditBean extends Serializable with LazyLogging {

	val MAX_LOGO_SIZE_PARAM = "me.jtalk.home.project.logo.size.max"
	val SCALED_LOGO_WIDTH_PX = 200

	@EJB
	private var editor: ProjectEdit = _

	@Inject
	private var projects: ProjectsHolderBean = _

	@Inject
	private var projectService: ProjectService = _

	@Inject
	private var config: Configuration = _

	@Inject
	private var onChange: Event[ProjectUpdated] = _

	@BeanProperty
	val nameParamter = "name"

	@BeanProperty
	val logo: ImageLoader = new ImageLoader("png")

	@BeanProperty
	var editingLinkName: String = _

	@BeanProperty
	var editingLinkUrl: String = _

	var editingLinkIndex: Option[Int] = None

	@PostConstruct
	def onCreate() = logger.debug("Project editing bean created") withCall load()

	def load(): Unit = Some(FacesContext.getCurrentInstance.getExternalContext.getRequest.asInstanceOf[HttpServletRequest])
			.mapEnsure(_.getParameter(nameParamter))
			.orElse(projects.getFirstProjectAll
					.map(_.getInternalName))
			.map(editor.loadByName _)
			.change(l => logger.debug("Project with name {} is {} for editing by JSF", editor.internalName, l.decide("loaded", "not loaded")))
			.exists(b => b)

	def load(id: String): Boolean = Option(id)
			.map(editor.load _)
			.change(l => logger.debug("Project with id {} is {} for editing by JSF", id, l.decide("loaded", "not loaded")))
			.exists(b => b)

	def get = editor.get
	def isLoaded = editor.isLoaded
	def isCurrent(name: String) = editor.get.getInternalName == name
	def getMaxLogoSize(): Long = Constants.MAX_PROJECT_LOGO_SIZE max config.getInt(MAX_LOGO_SIZE_PARAM, 0)
	def moveLeft(): Unit = withReload(id => projectService.moveLeft(id))
	def moveRight(): Unit = withReload(id => projectService.moveRight(id))

	def moveLinkUp(i: Int): Unit = Collections.swap(editor.get.getLinks, i, i - 1)
	def moveLinkDown(i: Int) = Collections.swap(editor.get.getLinks, i, i + 1)
	def addLink(name: String, url: String): Unit = editor.get.getLinks.add(new ProjectLink(name, url))
	def removeLink(i: Int) = Option(editor.get.getLinks).map(_.remove(i))
	def markLinkForEdit(i: Int): Unit = {
		editingLinkIndex = Some(i)
		val link = editor.get.getLinks.get(i)
		editingLinkName = link.getName
		editingLinkUrl = link.getUrl
		logger.debug("Link {} of the project {} is marked for editing: name {}, url {}", i.toString, editor.internalName, editingLinkName, editingLinkUrl)
	}
	def doneLinkEditing(): Unit = {
		editingLinkIndex.map(i => {
			val link = editor.get.getLinks.get(i)
			link.setName(editingLinkName)
			link.setUrl(editingLinkUrl)
			logger.debug("Link {} of the project {} changes applied: name {}, url {}", i.toString, editor.internalName, editingLinkName, editingLinkUrl)
		})
		editingLinkIndex.changeFalse(logger.warn("Project {} link editing failed: no index set", editor.internalName))
		editingLinkIndex = None
	}

	def editingLink(): Option[ProjectLink] = {
		editingLinkIndex.flatMap(i => {
			Option(editor.get.getLinks)
					.map(l => l.get(i))
		})
	}

	def save(): Unit = {
		logo.getWithWidth(SCALED_LOGO_WIDTH_PX)
				.map(editor.get.setLogo _)
		editingLinkIndex = None
		editor.save()
		onChange.fire(new ProjectUpdated)
	}

	def reset(): Unit = {
		val loaded = editor.get.getId
		clear()
		load(loaded)
	}

	def clear(): Unit = {
		logger.debug("Currently editing project {} cleanup requested", editor.internalName)
		editingLinkIndex = None
		editor.reset()
	}

	def delete(): Unit = {
		logger.info("Deleting project {}", editor.internalName)
		editingLinkIndex = None
		editor.delete()
		onChange.fire(new ProjectUpdated)
		projects.load()
		projects.getFirstProjectAll()
				.change(p => logger.debug("Project {} will be edited after the previous one's removal", p.getInternalName))
				.map(_.getId)
				.map(load _)
	}

	@PreDestroy
	def onDestroy(): Unit = {
		editor.remove()
	}

	protected def withReload[T](f: String => T): Unit = {
		val currentId = editor.get.getId
		clear()
		f(currentId)
		load(currentId)
		onChange.fire(new ProjectUpdated)
		projects.load()

	}
}
