/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import javax.annotation.{ManagedBean, PostConstruct}
import javax.faces.context.FacesContext
import javax.inject.{Inject, Named}
import javax.servlet.http.HttpServletRequest

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.exception.NotFoundException
import me.jtalk.home.faces.utils.NavigationParameterUtils
import me.jtalk.home.model.Article
import me.jtalk.home.service.ArticleService
import me.jtalk.home.utils.scala.EnhancedOption._
import org.omnifaces.cdi.ViewScoped

import scala.beans.BeanProperty

@Named
@ManagedBean
@ViewScoped
class PostViewBean extends Serializable with StrictLogging {

	@Inject
	private var navigationParams: NavigationParameterUtils = _

	@Inject
	private var articleService: ArticleService = _

	@Inject
	private var currentUser: CurrentUser = _

	@BeanProperty
	var article: Article = _

	@PostConstruct
	def load(): Unit = {
		article = Option(FacesContext.getCurrentInstance.getExternalContext.getRequest.asInstanceOf[HttpServletRequest])
				.mapEnsure(_.getParameter(navigationParams.blogPostNameParameter), _ => logger.warn("Blog entry requested without a name"))
				.flatMapWith(articleService.loadByNameReadOnly _, logger.warn("Non-existent project {} load requested", _))
				.getOrElse(throw new NotFoundException("Blog entry not found"))
	}

	def reload(): Unit = {
		article = Option(article)
				.map(_.getId)
				.flatMapWith(articleService.loadReadOnly _, logger.warn("Non-existent project {} reload requested", _))
				.filterOr(_.isPublished || currentUser.isAdmin, a => logger.warn("Non-published article {} requested by non-admin user", a.getInternalTitle))
				.getOrElse(throw new NotFoundException("Blog entry not found"))
	}
}
