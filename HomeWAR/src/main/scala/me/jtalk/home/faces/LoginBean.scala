/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import javax.annotation.ManagedBean
import javax.enterprise.inject.Instance
import javax.faces.application.FacesMessage
import javax.faces.component.UIViewRoot
import javax.inject.{Inject, Named}
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.faces.utils.{ErrorUtils, Urls}
import me.jtalk.home.utils.scala.EnhancedBoolean._
import org.omnifaces.cdi.ViewScoped

import scala.beans.BeanProperty
import scala.language.implicitConversions
import scala.util.Try

@Named
@ManagedBean
@ViewScoped
class LoginBean extends StrictLogging with Serializable {

	@Inject
	private var view: Instance[UIViewRoot] = _

	@Inject
	private var request: HttpServletRequest = _

	@Inject
	private var errors: ErrorUtils = _

	@Inject
	private var user: CurrentUser = _

	@BeanProperty
	var login: String = _

	@BeanProperty
	var password: String = _

	def doLogin() = {
		if (tryLogin()) {
			Urls.BIO_URL + "?faces-redirect=true"
		} else {
			null
		}
	}

	protected def tryLogin(): Boolean = {
		Try(request.login(login, password))
				.map(_ => checkAdmin())
				.recover(checkLoginException)
				.get
	}

	protected def checkLoginException(): PartialFunction[Throwable, Boolean] = {
		case e: ServletException => {
			logger.warn("Logging in failed for login {} from IP address {}", loginOrNull(), request.getRemoteAddr, e)
			errors.reportGlobalValidationError(FacesMessage.SEVERITY_ERROR, "Login failed", "Invalid login credentials")
			false
		}
	}

	protected def checkAdmin(): Boolean = user.isAdmin().toOption.getOrElse({
		logger.warn("Non-administrative user {} logged into an application", loginOrNull())
		request.logout()
		false
	})

	private def loginOrNull() = Option(login).getOrElse("<null>")
}
