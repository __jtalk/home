/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces.utils

import javax.annotation.ManagedBean
import javax.enterprise.context.ApplicationScoped
import javax.inject.{Inject, Named}

import org.apache.commons.configuration.Configuration

import scala.beans.BeanProperty

@Named("pageUtils")
@ManagedBean
@ApplicationScoped
class PageUtils {

	@Inject
	@BeanProperty
	var config: Configuration = _

	val subtitles = Map(
		(Urls.BIO_URL, "About me"),
		(Urls.PROJECTS_URL, "Projects"),
		(Urls.BLOG_URL, "Blog"),
		(Urls.POST_URL, "Blog"),
		("/admin/bio.xhtml", "Edit owner bio"),
		("/admin/projects.xhtml", "Edit projects"),
		("/admin/post.xhtml", "Edit blog post"),
		("/admin/blog.xhtml", "Edit blog entries"),
		("/admin/tags.xhtml", "Edit tags"),
		("/admin/imagess.xhtml", "Manage uploaded images")
	)

	def title() = config.getString("me.jtalk.home.title", "Home")

	def subtitle(location: String) = subtitles.get(location)
			.map(": " + _)
			.getOrElse("")
}
