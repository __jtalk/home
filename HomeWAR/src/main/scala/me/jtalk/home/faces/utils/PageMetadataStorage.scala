/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.jtalk.home.faces.utils

import javax.annotation.ManagedBean
import javax.enterprise.context.ApplicationScoped

@ManagedBean
@ApplicationScoped
class PageMetadataStorage {

	val pages = List(
		PageMetadata("About", Urls.BIO_URL),
		PageMetadata("Projects", Urls.PROJECTS_URL),
		PageMetadata("Blog", Urls.BLOG_URL)
	)

	def getMetadata = pages
}

case class PageMetadata(val name: String, location: String) {
}
