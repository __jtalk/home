/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import java.util.{List => JList}
import javax.annotation.{ManagedBean, PostConstruct}
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.event.{Observes, TransactionPhase}
import javax.inject.{Inject, Named}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.model.Project
import me.jtalk.home.service.ProjectService
import me.jtalk.home.service.event.ProjectUpdated

import scala.beans.BeanProperty

@Named
@ManagedBean
@ApplicationScoped
class ProjectsHolderBean extends StrictLogging {

	@Inject
	var projectService: ProjectService = _

	@BeanProperty
	var projects: JList[Project] = _ // Due to a bug in JSF we cannot use immutable collections here.

	@BeanProperty
	var projectsAll: JList[Project] = _ // Due to a bug in JSF we cannot use immutable collections here.

	@PostConstruct
	def load(): Unit = {
		projects = projectService.loadReadOnly(published = true)
		projectsAll = projectService.loadReadOnly()
		logger.debug("Projects list reloaded")
	}

	def getFirstProject(): Option[Project] = Some(projects)
	        .filterNot(_.isEmpty)
	        .map(_.get(0))

	def getFirstProjectAll(): Option[Project] = Some(projectsAll)
			.filterNot(_.isEmpty)
			.map(_.get(0))

	def reload(@Observes(during = TransactionPhase.AFTER_SUCCESS) e: ProjectUpdated) = load()
}
