/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import java.util.{ArrayList => JArrayList}
import javax.annotation.{ManagedBean, PostConstruct}
import javax.enterprise.event.Event
import javax.faces.application.FacesMessage
import javax.faces.component.UIInput
import javax.inject.{Inject, Named}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.faces.utils.{ErrorUtils, NavigationParameterUtils}
import me.jtalk.home.model.Article
import me.jtalk.home.service.ArticleService
import me.jtalk.home.service.event.ArticlesUpdated
import me.jtalk.home.utils.scala.EnhancedBoolean.enhanceBoolean
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption
import org.omnifaces.cdi.ViewScoped

import scala.beans.BeanProperty

@Named
@ManagedBean
@ViewScoped
class BlogEditBean extends Serializable with StrictLogging {

	@Inject
	private var articleService: ArticleService = _

	@Inject
	private var errors: ErrorUtils = _

	@Inject
	private var navigationParams: NavigationParameterUtils = _

	@Inject
	private var blogBean: BlogBean = _

	@Inject
	private var postChanged: Event[ArticlesUpdated] = _

	@BeanProperty
	var articles: JArrayList[Article] = _

	@PostConstruct
	def load(): Unit = {
		val loaded = articleService.loadAll();
		articles = new JArrayList(loaded)
	}

	def createArticle(title: String, internalTitle: String, internalTitleComponent: UIInput): String = articleService.create(title, internalTitle)
			.changeFalse(errors.reportValidationError(internalTitleComponent, FacesMessage.SEVERITY_ERROR, "Duplicating project", "Project with such name already exists"))
			.map(a => s"/admin/blog/post.xhtml?${navigationParams.blogPostNameParameter}=${a.getInternalTitle}")
			.change(_ => load())
			.change(_ => postChanged.fire(new ArticlesUpdated))
			.orNull

	def deleteArticle(id: String): Unit = articleService.delete(id)
			.toOption
			.change(_ => logger.debug("Article {} deleted successfully", id))
			.changeFalse(logger.warn("Unable to delete article {}", id))
			.changeFalse(errors.reportGlobalValidationError(FacesMessage.SEVERITY_WARN, "Article not found", "This article is already deleted"))
			.change(_ => blogBean.load())
			.change(_ => load())
			.change(_ => postChanged.fire(new ArticlesUpdated))
}
