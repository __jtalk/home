/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import javax.annotation.{PostConstruct, ManagedBean}
import javax.inject.{Inject, Named}
import java.util.{ArrayList => JArrayList}
import javax.transaction.Transactional

import com.typesafe.scalalogging.LazyLogging
import me.jtalk.home.service.{SpamService, OwnerService}
import org.omnifaces.cdi.ViewScoped

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

@Named
@ManagedBean
@ViewScoped
class SpamFilterEditBean extends Serializable with LazyLogging {

	@Inject
	private var owner: OwnerService = _

	@Inject
	private var spamService: SpamService = _

	@BeanProperty
	var spamPatterns: JArrayList[String] = _

	@BeanProperty
	var excludePatterns: JArrayList[String] = _

	@PostConstruct
	def load(): Unit = {
		spamPatterns = new JArrayList(spamService.loadSpamPatterns)
		excludePatterns = new JArrayList(spamService.loadExcludePatterns)
	}

	@Transactional
	def addSpamPattern(pattern: String): Unit = {
		val patterns = owner.load().getSpamSettings.getSpamPatterns
		if (!patterns.contains(pattern)) {
			patterns.add(pattern)
		}
		spamService.reload()
		load()
	}

	@Transactional
	def deleteSpamPattern(pattern: String): Unit = {
		owner.load().getSpamSettings.getSpamPatterns.remove(pattern)
		spamService.reload()
		load()
	}

	@Transactional
	def addExcludePattern(pattern: String): Unit = {
		val patterns = owner.load().getSpamSettings.getExcludedPatterns
		if (!patterns.contains(pattern)) {
			patterns.add(pattern)
		}
		spamService.reload()
		load()
	}

	@Transactional
	def deleteExcludePattern(pattern: String): Unit = {
		owner.load().getSpamSettings.getExcludedPatterns.remove(pattern)
		spamService.reload()
		load()
	}
}
