/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces.utils

import java.util
import javax.annotation.{PostConstruct, ManagedBean}
import javax.enterprise.context.ApplicationScoped
import javax.inject.{Inject, Named}

import me.jtalk.home.faces.ProjectsHolderBean
import me.jtalk.home.model.Project
import me.jtalk.home.service.OwnerService

import scala.collection.JavaConversions._

@Named
@ManagedBean
@ApplicationScoped
class Images {

	@Inject
	var projects: ProjectsHolderBean = _

	@Inject
	private var ownerService: OwnerService = _

	private var defaultPhoto: DefaultPhotoUtils = _

	@PostConstruct
	def init(): Unit = {
		defaultPhoto = new DefaultPhotoUtils
	}

	def logo(id: String): Array[Byte] = getProject(id)
			.map(_.getLogo)
			.getOrElse(null)

	def logoLastModified(id: String): Int = util.Arrays.hashCode(logo(id))

	def getOwnerPhoto(): Array[Byte] = Option(ownerService.loadReadOnly().getPhoto)
			.getOrElse(defaultPhoto.load)

	protected def getProject(id: String): Option[Project] = Some(projects)
			.map(_.projects)
			.flatMap(_.find(_.getId == id))
}
