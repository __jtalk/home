/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces.utils

import javax.annotation.ManagedBean
import javax.enterprise.context.SessionScoped
import javax.inject.{Inject, Named}

import scala.beans.BeanProperty

@Named("menuUtils")
@ManagedBean
@SessionScoped
class MenuUtils extends Serializable {

	@Inject
	@BeanProperty
	var metadata: PageMetadataStorage = _

	def getMenuItems(): Array[MenuItem] = metadata.getMetadata.toStream
			.map(m => MenuItem(m.name, m.location))
			.toArray
}

case class MenuItem(
		@BeanProperty val name: String,
		@BeanProperty val location: String) {
}
