/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces.utils.rss

import java.time.ZonedDateTime
import java.util.{Date, UUID}
import javax.annotation.{ManagedBean, PostConstruct}
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.event.{Observes, Reception, TransactionPhase}
import javax.enterprise.inject.Instance
import javax.inject.Inject

import com.codahale.metrics.annotation.Metered
import com.rometools.rome.feed.synd._
import me.jtalk.home.faces.utils.{BioUtils, BlogUtils, NavigationParameterUtils, Urls}
import me.jtalk.home.model.Article
import me.jtalk.home.service.ArticleService
import me.jtalk.home.service.event.ArticlesUpdated
import me.jtalk.home.utils.rss.FeedType
import org.apache.commons.configuration.Configuration

import scala.collection.JavaConversions._

@ManagedBean
@ApplicationScoped
class RssUtils {

	private final val FEED_AUTHOR_PARAM = "me.jtalk.home.rss.author"
  private final val FEED_CATEGORIES_PARAM = "me.jtalk.home.rss.feed.categories"

	private final val FEED_TITLE_PARAM = "me.jtalk.home.rss.feed.title"
	private final val FEED_TITLE_PARAM_DEFAULT = ""

	private final val FEED_DESCRIPTION_PARAM = "me.jtalk.home.rss.feed.description"
	private final val FEED_DESCRIPTION_PARAM_DEFAULT = ""

	private final val FEED_SIZE_PARAM = "me.jtalk.home.rss.feed.size"
	private final val FEED_SIZE_PARAM_DEFAULT = 20

	@Inject
	private var owner: BioUtils = _

	@Inject
	private var articles: ArticleService = _

	@Inject
	private var config: Configuration = _

	@Inject
	private var blogUtils: BlogUtils = _

	@Inject
	private var uuid: Instance[UUID] = _

	@Inject
	private var navigationParams: NavigationParameterUtils = _

	private var dataCache: List[Article] = _
	private var feedUuid = ""

	@PostConstruct
	def load(): Unit = {
		dataCache = articles.loadPageReadOnly(0, feedSize()).toList
		feedUuid = uuid.get().toString
	}

	/**
	  * Create RSS feed specific to the urlFormatter specified
	  *
	  * @param feedType RSS/Atom feed type to produce
    * @param previewOnly Show preview instead of a full article
	  * @param localToGlobalUrlFormatter function taking article's internal name and returning RSS-friendly URL to that article
	  *
	  * @return feed object
	  */
	@Metered(name="rss-utils.feed.creation", absolute = true)
	def makeFeed(feedType: FeedType, previewOnly: Boolean, localToGlobalUrlFormatter: String => String): SyndFeed = {
		val postUrlFormatter: String => String = article => localToGlobalUrlFormatter(s"${Urls.POST_URL}?${navigationParams.blogPostNameParameter}=${article}")
		val entries = createFeedEntries(dataCache, previewOnly, postUrlFormatter)
		feed(feedType, feedAuthor(), feedTitle(), feedDescription(), feedUuid, localToGlobalUrlFormatter(""), feedCategories(), entries)
	}

	def onBlogChange(@Observes(during = TransactionPhase.AFTER_SUCCESS, notifyObserver = Reception.IF_EXISTS) e: ArticlesUpdated): Unit = load()

	protected def feedTitle(): String = config.getString(FEED_TITLE_PARAM, FEED_TITLE_PARAM_DEFAULT)
	protected def feedAuthor(): String = config.getString(FEED_AUTHOR_PARAM, owner.getOwnerName)
	protected def feedDescription(): String = config.getString(FEED_DESCRIPTION_PARAM, FEED_DESCRIPTION_PARAM_DEFAULT)
	protected def feedSize(): Int = config.getInt(FEED_SIZE_PARAM, FEED_SIZE_PARAM_DEFAULT)
  protected def feedCategories(): List[SyndCategory] = config.getStringArray(FEED_CATEGORIES_PARAM).toList.map(name => {
    val category = new SyndCategoryImpl
    category.setName(name)
    category
  })

	protected def createFeedEntries(data: List[Article], previewOnly: Boolean, urlFormatter: String => String): List[SyndEntry] = {
		val author = feedAuthor()
		data
				.map(a => entry(
					author,
					a.getTitle,
          createContent(a, previewOnly, urlFormatter),
					"text/html",
					urlFormatter(a.getInternalTitle),
					a.getCreated))
	}

  protected def createContent(a: Article, previewOnly: Boolean, urlFormatter: String => String): String = if (previewOnly) {
    addReadFurther(blogUtils.previewContent(a), urlFormatter(a.getInternalTitle))
  } else {
    blogUtils.fullContent(a)
  }

	protected def addReadFurther(content: String, url: String): String = s"""${content}<a href="//${url}">Read further</a>"""

	protected def entry(author: String, title: String, content: String, contentType: String, link: String, published: ZonedDateTime): SyndEntry = {

		val desc = new SyndContentImpl
		desc.setType(contentType)
		desc.setValue(content)

		val e = new SyndEntryImpl
		e.setAuthor(author)
		e.setTitle(title)
		e.setLink(link)
		e.setPublishedDate(Date.from(published.toInstant))
		e.setDescription(desc)
		e
	}

	protected def feed(feedType: FeedType, author: String, title: String, description: String, uri: String, link: String, categories: List[SyndCategory], entries: List[SyndEntry]): SyndFeed = {
		val f = new SyndFeedImpl
		f.setFeedType(feedType.getCode)
		f.setAuthor(author)
		f.setTitle(title)
		f.setDescription(description)
		f.setUri(uri)
		f.setLink(link)
		f.setEntries(entries)
		f.setCategories(categories)
		f
	}
}
