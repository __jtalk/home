/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces.validator

import javax.annotation.ManagedBean
import javax.enterprise.context.ApplicationScoped
import javax.faces.application.FacesMessage
import javax.faces.component.UIComponent
import javax.faces.context.FacesContext
import javax.faces.validator.{ValidatorException, Validator}
import javax.inject.{Inject, Named}

import com.typesafe.scalalogging.{StrictLogging, LazyLogging}
import me.jtalk.home.faces.utils.CaptchaHolder

@Named
@ManagedBean
@ApplicationScoped
class CaptchaValidator extends Validator with StrictLogging {

	@Inject
	private var captcha: CaptchaHolder = _

	override def validate(context: FacesContext, component: UIComponent, value: scala.Any): Unit = {
		if (!captcha.isRequired) {
			logger.debug("Captcha check not required, skipping")
		} else if (!captcha.check(value.asInstanceOf[String])) {
			logger.warn("Captcha check failed")
			throw new ValidatorException(errorMessage("Captcha does not match"))
		} else {
			captcha.approve()
			captcha.clear()
			logger.debug("Captcha check passed")
		}
	}

	protected def errorMessage(message: String): FacesMessage = {
		val msg = new FacesMessage(message, message)
		msg.setSeverity(FacesMessage.SEVERITY_FATAL)
		msg;
	}
}
