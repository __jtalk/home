/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import javax.annotation.ManagedBean
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.Instance
import javax.faces.context.ExternalContext
import javax.inject.{Inject, Named}

import me.jtalk.home.configuration.Roles

import scala.beans.BeanProperty

@Named
@ManagedBean
@ApplicationScoped
class CurrentUser {

	@Inject
	@BeanProperty
	var extCtx: Instance[ExternalContext] = _

	def isAdmin() = extCtx.get.isUserInRole(Roles.ADMINISTRATOR)
	def isLogged() = isAdmin() // Semantically different so their separation might save us in the future
}
