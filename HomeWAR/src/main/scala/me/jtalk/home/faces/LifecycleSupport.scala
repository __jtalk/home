/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import javax.ejb._
import javax.inject.Inject

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.faces.utils.rss.RssUtils
import me.jtalk.home.faces.utils.{BioUtils, LatestPostsUtils}
import me.jtalk.home.service.SpamService

@Startup
@Singleton
@LocalBean
@ConcurrencyManagement(ConcurrencyManagementType.CONTAINER)
class LifecycleSupport extends StrictLogging {

	@Inject
	private var bio: BioUtils = _

	@Inject
	private var projectsHolder: ProjectsHolderBean = _

	@Inject
	private var rss: RssUtils = _

	@Inject
	private var tagsBean: TagsBean = _

	@Inject
	private var latestPosts: LatestPostsUtils = _

	@Inject
	private var spamService: SpamService = _

	/**
	  * Update caches from the database sometimes.
	  *
	  * A good trade-off between performance and in-database editing support.
	  */
	@Schedule(hour = "*", minute = "*/10", persistent = false)
	def tick() = {
		bio.load()
		projectsHolder.load()
		tagsBean.load()
		rss.load()
		latestPosts.load()
		spamService.reload()
		logger.debug("Lifecycle support has reloaded all caches")
	}
}
