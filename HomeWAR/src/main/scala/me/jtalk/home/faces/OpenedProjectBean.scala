/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import java.util
import java.util.{List => JList}
import javax.annotation.{ManagedBean, PostConstruct}
import javax.faces.context.FacesContext
import javax.inject.{Inject, Named}
import javax.servlet.http.HttpServletRequest

import me.jtalk.home.exception.NotFoundException
import me.jtalk.home.faces.utils.{NavigationParameterUtils, RenderingUtils}
import me.jtalk.home.model.{Project, ProjectLink}
import me.jtalk.home.utils.scala.EnhancedOption._
import org.omnifaces.cdi.ViewScoped

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

@Named
@ManagedBean
@ViewScoped
class OpenedProjectBean extends Serializable {

	@Inject
	private var projectsBean: ProjectsHolderBean = _

	@Inject
	private var renderingUtils: RenderingUtils = _

	@Inject
	private var navigationParams: NavigationParameterUtils = _

	@BeanProperty
	var current: Project = _

	@BeanProperty
	var currentDescriptionHtml: String = ""

	@PostConstruct
	def load(): Unit = {
		current = Some(FacesContext.getCurrentInstance.getExternalContext.getRequest.asInstanceOf[HttpServletRequest])
				.mapEnsure(_.getParameter(navigationParams.projectNameParameter))
				.flatMap(name => projectsBean.projects.find(_.getInternalName == name))
				.orElse(projectsBean.getFirstProject)
		        .getOrElse(throw new NotFoundException("No projects found"))
		currentDescriptionHtml = Some(current)
				.mapEnsure(_.getDescription)
				.map(renderingUtils.render _)
				.getOrElse("")
	}

	def isCurrent(internalName: String): Boolean = Some(current)
			.mapEnsure(_.getInternalName)
			.filter(_ == internalName)
			.isDefined

	def getLinks(): JList[ProjectLink] = Some(current)
			.mapEnsure(_.getLinks)
			.map(new util.ArrayList(_))
			.getOrElse(null)
}
