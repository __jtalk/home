/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces.utils

import java.util
import javax.annotation.{ManagedBean, PostConstruct}
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.event.{Observes, TransactionPhase}
import javax.enterprise.inject.Instance
import javax.inject.{Inject, Named}
import javax.transaction.Transactional

import me.jtalk.home.configuration.Constants
import me.jtalk.home.faces.BioEditBean
import me.jtalk.home.model.Owner
import me.jtalk.home.service.OwnerService
import me.jtalk.home.service.event.BioUpdated

import scala.beans.BeanProperty

@Named
@ManagedBean
@ApplicationScoped
class BioUtils extends Serializable {

	@Inject
	private var ownerService: OwnerService = _

	@Inject
	private var renderingUtils: RenderingUtils = _

	@BeanProperty
	var content = ""

	@BeanProperty
	var contentRaw = ""

	@BeanProperty
	var ownerName = ""

	@BeanProperty
	var ownerNickName = ""

	@BeanProperty
	var ownerEmail = ""

	@BeanProperty
	var ownerDescription = ""

	@BeanProperty
	var photoLastModified = 0

	@PostConstruct
	def load() {
		val owner = ownerService.loadReadOnly()
		update(owner)
	}

	def reload(@Observes(during = TransactionPhase.AFTER_SUCCESS) e: BioUpdated): Unit = {
		load()
	}

	protected def update(owner: Owner) {
		photoLastModified = util.Arrays.hashCode(owner.getPhoto);
		ownerName = owner.getEntries.get(Constants.OWNER_USER_NAME_KEY)
		ownerEmail = owner.getEntries.get(Constants.OWNER_USER_EMAIL_KEY)
		ownerNickName = owner.getEntries.get(Constants.OWNER_USER_NICKNAME_KEY)
		ownerDescription = owner.getEntries.get(Constants.OWNER_USER_DESCRIPTION_KEY)
		contentRaw = owner.getEntries.get(Constants.OWNER_USER_BIO_KEY)
		content = renderingUtils.render(contentRaw)
	}
}
