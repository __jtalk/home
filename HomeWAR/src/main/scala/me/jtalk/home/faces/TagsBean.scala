/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import javax.annotation.{PostConstruct, ManagedBean}
import javax.enterprise.context.ApplicationScoped
import javax.inject.{Inject, Named}
import java.util.{ArrayList => JArrayList}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.model.ArticleTag
import me.jtalk.home.service.TagService

import scala.beans.BeanProperty

@Named
@ManagedBean
@ApplicationScoped
class TagsBean extends StrictLogging {

	@Inject
	private var tagService: TagService = _

	@BeanProperty
	var tags: JArrayList[ArticleTag] = _

	@PostConstruct
	def load(): Unit = {
		val loaded = tagService.loadReadOnly();
		tags = new JArrayList(loaded)
	}
}
