/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.jtalk.home.faces.utils

import javax.annotation.ManagedBean
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.inject.Instance
import javax.faces.application.FacesMessage
import javax.faces.component.{UIForm, UIComponent, UIInput}
import javax.faces.context.FacesContext
import javax.inject.{Named, Inject}

import scala.beans.BeanProperty

@Named
@ManagedBean
@ApplicationScoped
class ErrorUtils extends Serializable {

	@Inject
	@BeanProperty
	var ctx: Instance[FacesContext] = _

	def reportValidationError(input: UIInput, severity: FacesMessage.Severity, brief: String, detail: String) = {
		val facesCtx: FacesContext = ctx.get
		input.setValid(false)
		facesCtx.addMessage(input.getClientId(facesCtx), new FacesMessage(severity, brief, detail))
		facesCtx.validationFailed
	}

	def reportGlobalValidationError(severity: FacesMessage.Severity, brief: String, detail: String) = {
		val facesCtx: FacesContext = ctx.get
		facesCtx.addMessage(null, new FacesMessage(severity, brief, detail))
		facesCtx.validationFailed
	}

	def formClass(form: UIForm): String = (form.isSubmitted, FacesContext.getCurrentInstance.isValidationFailed()) match {
			case (true, true) => "error"
			case (true, false) => "success"
			case (false, _) => ""
	}
}