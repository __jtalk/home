/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import javax.annotation.ManagedBean
import javax.enterprise.context.RequestScoped
import javax.faces.application.FacesMessage
import javax.faces.component.UIInput
import javax.inject.{Inject, Named}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.faces.utils.ErrorUtils
import me.jtalk.home.model.Project
import me.jtalk.home.service.ProjectService
import me.jtalk.home.utils.scala.EnhancedOption._

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

@Named
@ManagedBean
@RequestScoped
class ProjectCreateBean extends StrictLogging {

	@Inject
	private var projects: ProjectService = _

	@Inject
	private var projectsList: ProjectsHolderBean = _

	@Inject
	private var projectEditBean: ProjectEditBean = _

	@Inject
	private var errors: ErrorUtils = _

	@BeanProperty
	var name: String = _

	@BeanProperty
	var internalName: String = _

	@BeanProperty
	var internalNameInput: UIInput = _

	def create(): String = {
		val p: Project = createProjectEntity()
		if (projects.create(p)) {
			projectEditBean.clear()
			if (!projectEditBean.load(p.getId)) {
				throw new IllegalStateException(s"Unable to load a project created for ${internalName} with id ${p.getId}")
			}
			projectsList.load()
			logger.debug("Project named {} has been created", internalName)
			s"/admin/projects.xhtml?${projectEditBean.nameParamter}=${p.getInternalName}&faces-redirect=true"
		} else {
			errors.reportValidationError(internalNameInput, FacesMessage.SEVERITY_ERROR, "Duplicate name", s"The internal name ${internalName} already exists")
			logger.warn("Unable to create project named {}", internalName)
			null
		}
	}

	protected def createProjectEntity(): Project = {
		val p = new Project
		p.setName(name)
		p.setInternalName(internalName)
		p.setOrder(nextOrder)
		p
	}

	protected def nextOrder(): Int = Option(projectsList.projects)
			.flatMap(_.lastOption)
			.map(_.getOrder)
			.map(_ + 1)
			.orElse(Some(0))
			.change(n => logger.debug("The next order for the new project is {}", n.toString))
	        .get
}
