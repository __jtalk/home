/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.faces

import javax.annotation.ManagedBean
import javax.inject.{Inject, Named}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.service.TagService
import me.jtalk.home.utils.scala.EnhancedAny.enhanceAny
import org.omnifaces.cdi.ViewScoped

import scala.beans.BeanProperty
import scala.collection.mutable

@Named
@ManagedBean
@ViewScoped
class TagsEditBean extends Serializable with StrictLogging {

	@Inject
	private var service: TagService = _

	@Inject
	private var tagsBean: TagsBean = _

	@BeanProperty
	var newTagName: String = _

	def create(): Unit = {
		if (service.createTag(newTagName)) {
			tagsBean.load()
		}
		newTagName = ""
	}
	def remove(name: String): Unit = {
		if (service.deleteTag(name)) {
			tagsBean.load()
		}
	}

	def count(name: String): Long = service.countArticles(name)
}
