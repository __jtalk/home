/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.utils;

import lombok.SneakyThrows;
import lombok.val;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import javax.annotation.ManagedBean;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

@ManagedBean
@ApplicationScoped
public class ImageUrlEnhancer {

	@Inject
	private HttpServletRequest servletRequest;

	/**
	 * Prepend all the images with relative src with a context path value.
	 *
	 * @param data the source HTML
	 * @return the resulting HTML
	 * @throws URISyntaxException if any image src on this page contains an invalid URI value.
	 */
	public String enhanceAll(String data) {

		val parsed = Jsoup.parse(data);
		val images = parsed.body().getElementsByTag("img");
		for (Element e : images) {
			if (isRelative(e)) {
				addContextRoot(e);
			}
		}
		// Keep newlines for prewrap on the resulting page
		parsed.outputSettings().prettyPrint(false);

		return parsed.toString();
	}

	@SneakyThrows(URISyntaxException.class)
	protected boolean isRelative(Element elem) {
		String url = elem.attr("src");
		return !new URI(url).isAbsolute();
	}

	protected void addContextRoot(Element elem) {
		val url = elem.attr("src");
		val withRoot = servletRequest.getContextPath() + url;
		elem.attr("src", withRoot)
		;
	}
}
