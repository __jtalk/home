/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.utils;


import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.jsoup.Jsoup;

import javax.annotation.ManagedBean;
import javax.enterprise.context.ApplicationScoped;

@Slf4j
@ManagedBean
@ApplicationScoped
public class SearchContentPurifier {

	public String purify(String content) {
		String plain = purifyHtml(content);
		return purifyPlain(plain);
	}

	public String purifyPlain(String plain) {
		return plain
				.toLowerCase()
				.replaceAll("(\\r|\\n)", " ") // Remove line breaks as they're not expected to be presented in a search expression
				.replaceAll("\\s", " ") // Normalize spaces
				.replace("  ", " "); // Remove space duplicates
	}

	protected String purifyHtml(String content) {
		val parsed = Jsoup.parse(content);
		return parsed.text();
	}
}
