/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.utils.rss;

import java.util.Arrays;
import java.util.Optional;

public enum FeedType {
	RSS_10("rss_1.0", false),
	RSS_20("rss_2.0", false),
	ATOM_03("atom_0.3", true),
	ATOM_10("atom_1.0", true);

	private final String code;
	private final boolean atom;

	FeedType(String code, boolean atom) {
		this.code = code;
		this.atom = atom;
	}

	public String getCode() {
		return code;
	}

	public boolean isAtom() {
		return atom;
	}

	public static Optional<FeedType> fromCode(String code) {
		return Arrays.stream(values())
				.filter(t -> t.getCode().equals(code))
				.findFirst();
	}
}
