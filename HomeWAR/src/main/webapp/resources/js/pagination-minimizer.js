/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function minimizePaginationMenu(menuElementRaw, omissionItemWindow) {

	var findCurrent = function (element) {
		var current = element.children('a.active.item').index();
		return current;
	};

	var determineMinimizationPolicy = function (currentNumber, maxSize) {
		var OMISSION_ITEM_SIZE = 1;
		var MIN_SIZE_TO_REDUCE = omissionItemWindow * 2 + OMISSION_ITEM_SIZE;
		var policy = [];
		if (maxSize - currentNumber > MIN_SIZE_TO_REDUCE) {
			var rightPolicy = {
				fromItem: currentNumber + omissionItemWindow,
				size: maxSize - currentNumber - 2 * omissionItemWindow
			};
			policy.push(rightPolicy);
		}
		if (currentNumber + 1 > MIN_SIZE_TO_REDUCE) {
			var leftPolicy = {
				fromItem: omissionItemWindow - 1,
				size: currentNumber - 2 * omissionItemWindow + 1
			};
			policy.push(leftPolicy);
		}
		if (policy.length > 0) {
			return policy;
		} else {
			return undefined;
		}
	};

	var minimize = function (element, policies) {
		policies.forEach(function (policy) {
			var from = policy.fromItem;
			var size = policy.size;
			element.children('a.item').each(function (i) {
				if (i >= from + size) {
					$(this).before('<div class="disabled item">...</div>');
					return false;
				}
				if (i > from) {
					$(this).remove();
				} else {
				}
			});
		});
	};

	if (omissionItemWindow === undefined) {
		omissionItemWindow = 2;
	}
	var menuElement = $(menuElementRaw);
	var currentNumber = findCurrent(menuElement);
	var maxSize = menuElement.children('a.item').size();
	var policy = determineMinimizationPolicy(currentNumber, maxSize);
	if (policy) {
		minimize(menuElement, policy);
	}
}

jQuery.fn.extend({
	paginationMinimize: function (itemToKeep) {
		$(this).each(function (i) {
			minimizePaginationMenu(this, itemToKeep);
		});
	}
});