/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Toggles 'collapsed' class on the element next to the clicked one's parent.
 * @param clicked Element that has been clicked by this event.
 * @returns undefined
 */
function toggleDropdown(clicked) {
	var row = $(clicked).parent();
	var jsData = row.next();
	if (jsData.hasClass('dropdown-data')) {
		jsData.toggleClass('collapsed');
	} else {
		alert('JSON data row is missing! This is a bug, please, contact the developer.');
	}
}

jQuery.fn.extend({
	tableDropdown: function () {
		$(this).each(function (i) {
			var element = this;
			this.onclick = function () {
				toggleDropdown(element);
			}
		});
	}
});