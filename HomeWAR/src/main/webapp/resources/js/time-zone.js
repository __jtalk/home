/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Created by jtalk on 26.03.16.
 */

var TimeZone = {
	toLocal: function (element) {
		var holder = $(element);
		if (!holder.hasClass('local-date-time-handled')) {
			var dateString = Number(holder.text().trim());
			var dateInstance = new Date(dateString);
			var local = dateInstance.toLocaleString('en', {
				day: 'numeric',
				month: 'long',
				year: 'numeric',
				hour: 'numeric',
				minute: 'numeric',
				second: 'numeric'
			});
			holder.text(local);
			holder.addClass('local-date-time-handled');
		}
	},
	toLocalShort: function (element) {
		var holder = $(element);
		if (!holder.hasClass('local-date-time-handled')) {
			var dateString = Number(holder.text().trim());
			var dateInstance = new Date(dateString);
			var local = dateInstance.toLocaleString('en', {
				day: 'numeric',
				month: 'long',
				year: 'numeric'
			});
			holder.text(local);
			holder.addClass('local-date-time-handled');
		}
	}
};

jQuery.fn.extend({
	toLocalDateTime: function () {
		$(this).each(function () {
			TimeZone.toLocal(this);
		});
	},
	toLocalDateTimeShort: function () {
		$(this).each(function () {
			TimeZone.toLocalShort(this);
		});
	}
});