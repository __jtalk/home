/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.jtalk.home.utils;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

public class QueryUtils {

	public static String escapeLike(String value) {
		return StringUtils.replaceEach(value,
				new String[]{"%", "\\\\%"},
				new String[]{"\\\\%", "\\\\_"});
	}

	public static String likePattern(String pattern) {
		return "%" + escapeLike(pattern) + "%";
	}

	public static ZonedDateTime fromDate(Date value) {
		return ZonedDateTime.ofInstant(value.toInstant(), ZoneId.of("UTC"));
	}

	public static Date toDate(ZonedDateTime value) {
		Instant zonedInstant = value
				.withZoneSameLocal(ZoneId.of("UTC"))
				.toInstant();
		return Date.from(zonedInstant);
	}

	public static <T, V> long countWith(EntityManager em, Class<T> entityClass, SingularAttribute<? super T, V> field, V fieldValue) {
		CriteriaBuilder b = em.getCriteriaBuilder();
		CriteriaQuery<Long> query = b.createQuery(Long.class);
		Root<T> groups = query.from(entityClass);
		query
				.select(b.count(groups))
				.where(b.equal(groups.get(field), fieldValue));
		return em.createQuery(query).getSingleResult();
	}

	public static <T> long count(EntityManager em, Class<T> entityClass) {
		CriteriaBuilder b = em.getCriteriaBuilder();
		CriteriaQuery<Long> query = b.createQuery(Long.class);
		Root<T> groups = query.from(entityClass);
		query.select(b.count(groups));
		return em.createQuery(query).getSingleResult();
	}

	public static <T, V> boolean exists(EntityManager em, Class<T> entityClass, SingularAttribute<? super T, V> field, V fieldValue) {
		return countWith(em, entityClass, field, fieldValue) > 0;
	}

	public static <T, V> List<T> findByUniqueField(EntityManager em, Class<T> entityClass, SingularAttribute<? super T, V> field, V fieldValue) {
		CriteriaBuilder b = em.getCriteriaBuilder();
		CriteriaQuery<T> query = b.createQuery(entityClass);
		Root<T> root = query.from(entityClass);
		query.where(b.equal(root.get(field), fieldValue));
		return em.createQuery(query).getResultList();
	}

	public static <T> List<T> findAll(EntityManager em, Class<T> entityClass) {
		CriteriaBuilder b = em.getCriteriaBuilder();
		CriteriaQuery<T> query = b.createQuery(entityClass);
		query.from(entityClass);
		return em.createQuery(query).getResultList();
	}

}
