/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.model;

import me.jtalk.home.configuration.Constants;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Project implements Serializable {

	@Id
	private String id;

	@Version
	private long version;

	@Column(length = Constants.MAX_PROJECT_NAME)
	private String name;

	@Column(length = Constants.MAX_PROJECT_NAME, unique = true)
	private String internalName;

	@Column(length = Constants.MAX_PROJECT_DESCRIPTION)
	private String description;

	@Column(length = Constants.MAX_PROJECT_LOGO_SIZE)
	private byte[] logo;

	@ElementCollection(fetch = FetchType.EAGER) // We usually need this entity entirely
	@OrderColumn(name = "\"order\"")
	@CollectionTable(
			name = "ProjectLink",
			joinColumns = @JoinColumn(name = "\"projectId\"", updatable = false, nullable = false))
	private List<ProjectLink> links = new ArrayList<>();

	private int order;
	private boolean published = false;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInternalName() {
		return internalName;
	}

	public void setInternalName(String internalName) {
		this.internalName = internalName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getLogo() {
		return logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public List<ProjectLink> getLinks() {
		return links;
	}

	public void setLinks(List<ProjectLink> links) {
		this.links = links;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}
}
