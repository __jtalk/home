/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import me.jtalk.home.configuration.Constants;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@EqualsAndHashCode(of = "id")
@NamedEntityGraphs({
		@NamedEntityGraph(name = Owner.GRAPH_WITH_ENTRIES, attributeNodes = {@NamedAttributeNode("entries")})
})
public class Owner implements Serializable {

	public static final int DEFAULT_ID = 0;

	public static final String GRAPH_WITH_ENTRIES = "Graph:Owner.entries";

	@Id
	private int id = DEFAULT_ID;

	@Version
	private long version;

	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(name = "\"OwnerData\"", joinColumns = @JoinColumn(name = "\"ownerId\"", updatable = false, nullable = false))
	@MapKeyColumn(name = "name", length = Constants.OWNER_DATA_KEY_SIZE, updatable = false, nullable = false)
	@Column(name = "value", length = Constants.OWNER_DATA_VALUE_SIZE)
	private Map<String, String> entries = new HashMap<>();

	@Size(max = Constants.MAX_PHOTO_SIZE)
	@Column(length = Constants.MAX_PHOTO_SIZE)
	private byte[] photo;

	@OneToOne(mappedBy = "owner", fetch = FetchType.LAZY)
	private SpamSettings spamSettings;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(
			name = "FooterLinks",
			joinColumns = @JoinColumn(name = "\"ownerId\""))
	@OrderColumn(name = "\"order\"")
	private List<FooterLink> footerLinks = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public Map<String, String> getEntries() {
		return entries;
	}

	public void setEntries(Map<String, String> entries) {
		this.entries = entries;
	}

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	public SpamSettings getSpamSettings() {
		return spamSettings;
	}

	public void setSpamSettings(SpamSettings spamSettings) {
		this.spamSettings = spamSettings;
	}

	public List<FooterLink> getFooterLinks() {
		return footerLinks;
	}

	public void setFooterLinks(List<FooterLink> footerLinks) {
		this.footerLinks = footerLinks;
	}
}
