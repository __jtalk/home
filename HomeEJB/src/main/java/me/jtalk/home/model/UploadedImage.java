/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.model;

import lombok.NoArgsConstructor;
import me.jtalk.home.configuration.Constants;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

@Entity
@NoArgsConstructor
@NamedEntityGraphs({
		@NamedEntityGraph(
				name = UploadedImage.LOAD_METADATA_GRAPH,
				attributeNodes = {
						@NamedAttributeNode("id"),
						@NamedAttributeNode("description"),
						@NamedAttributeNode("uploaded"),
				}
		),
		@NamedEntityGraph(
				name = UploadedImage.LOAD_CONTENT_GRAPH,
				attributeNodes = {
						@NamedAttributeNode("id"),
						@NamedAttributeNode("content"),
				}
		),
})
public class UploadedImage {

	public static final String LOAD_METADATA_GRAPH = "loadMetadata";
	public static final String LOAD_CONTENT_GRAPH = "loadContent";

	@Id
	private String id;

	@Column(length = Constants.MAX_UPLOADED_IMAGE_DESCRIPTION, nullable = true, updatable = false)
	private String description;

	@Column
	private ZonedDateTime uploaded;

	@NotNull
	@Column(length = Constants.MAX_UPLOADED_IMAGE_SIZE, nullable = false, updatable = false)
	private byte[] content;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ZonedDateTime getUploaded() {
		return uploaded;
	}

	public void setUploaded(ZonedDateTime uploaded) {
		this.uploaded = uploaded;
	}

	public byte[] getContent() {
		return content;
	}

	public void setContent(byte[] content) {
		this.content = content;
	}

	@PrePersist
	public void onCreate() {
		ZonedDateTime now = ZonedDateTime.now();
		setUploaded(now);
	}
}
