/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
public class SpamSettings implements Serializable {

	@Id
	public int id;

	@OneToOne
	@JoinColumn(name = "\"ownerId\"")
	private Owner owner;

	@ElementCollection(fetch = FetchType.LAZY)
	@Column(name = "\"pattern\"", nullable = false)
	@CollectionTable(
			name = "SpamPatterns",
			joinColumns = @JoinColumn(name = "\"settingsId\"", nullable = false, updatable = false))
	private Set<String> spamPatterns = new HashSet<>();

	@ElementCollection(fetch = FetchType.LAZY)
	@Column(name = "\"pattern\"", nullable = false)
	@CollectionTable(
			name = "SpamExcludePatterns",
			joinColumns = @JoinColumn(name = "\"settingsId\"", nullable = false, updatable = false))
	private Set<String> excludedPatterns = new HashSet<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public Set<String> getSpamPatterns() {
		return spamPatterns;
	}

	public void setSpamPatterns(Set<String> spamPatterns) {
		this.spamPatterns = spamPatterns;
	}

	public Set<String> getExcludedPatterns() {
		return excludedPatterns;
	}

	public void setExcludedPatterns(Set<String> excludedPatterns) {
		this.excludedPatterns = excludedPatterns;
	}
}
