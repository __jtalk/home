/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.jtalk.home.model;

import lombok.EqualsAndHashCode;
import me.jtalk.home.configuration.Constants;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@EqualsAndHashCode(of = "id")
public class Article {

	@Id
	private String id;

	@Version
	private long version;

	@NotNull
	@Size(min = 1)
	@Column(nullable = false)
	private String title;

	@NotNull
	@Size(min = 1)
	@Column(nullable = false, unique = true)
	private String internalTitle;

	@Column
	private boolean published = false;

	@Column
	private ZonedDateTime created;

	@Column
	private ZonedDateTime updated;

	@NotNull
	@Size(max = Constants.MAX_ARTICLE_SIZE)
	@Column(length = Constants.MAX_ARTICLE_SIZE, nullable = false)
	private String content;

	@NotNull
	@Size(max = Constants.MAX_ARTICLE_SIZE)
	@Column(length = Constants.MAX_ARTICLE_SIZE, nullable = false)
	private String purifiedContent;

	@NotNull
	@ManyToMany(mappedBy = "articles", fetch = FetchType.EAGER)
	private Set<ArticleTag> tags = new HashSet<>();

	@NotNull
	@OrderBy("posted DESC")
	@OneToMany(mappedBy = "article", cascade = {CascadeType.REMOVE}, fetch = FetchType.EAGER)
	private List<ArticleComment> comments = new ArrayList<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getInternalTitle() {
		return internalTitle;
	}

	public void setInternalTitle(String internalTitle) {
		this.internalTitle = internalTitle;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public ZonedDateTime getCreated() {
		return created;
	}

	public void setCreated(ZonedDateTime created) {
		this.created = created;
	}

	public ZonedDateTime getUpdated() {
		return updated;
	}

	public void setUpdated(ZonedDateTime updated) {
		this.updated = updated;
	}

	public String getContent() {
		return content;
	}

	/**
	 * We need to keep a normalized version of every article for searching
	 * purposes. We may use Elastic for indexing instead, but it's an overkill
	 * for such a simple application.
	 *
	 * @param content Markdown article markup
	 * @deprecated use setContent(String, String) instead
	 */
	@Deprecated
	public void setContent(String content) {
		this.content = content;
	}

	public String getPurifiedContent() {
		return purifiedContent;
	}

	/**
	 * We need to keep a normalized version of every article for searching
	 * purposes. We may use Elastic for indexing instead, but it's an overkill
	 * for such a simple application.
	 *
	 * @param purifiedContent Markdown article markup
	 * @deprecated use setContent(String, String) instead
	 */
	@Deprecated
	public void setPurifiedContent(String purifiedContent) {
		this.purifiedContent = purifiedContent;
	}

	public Set<ArticleTag> getTags() {
		return tags;
	}

	public void setTags(Set<ArticleTag> tags) {
		this.tags = tags;
	}

	public List<ArticleComment> getComments() {
		return comments;
	}

	public void setComments(List<ArticleComment> comments) {
		this.comments = comments;
	}

	@SuppressWarnings("deprecated")
	public void setContent(String content, String normalizedContent) {
		setContent(content);
		setPurifiedContent(normalizedContent);
	}

	@PrePersist
	public void onCreate() {
		ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
		setCreated(now);
		setUpdated(now);
	}

	@PreUpdate
	public void onChange() {
		ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
		setUpdated(now);
	}
}
