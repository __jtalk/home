/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.model;

import lombok.EqualsAndHashCode;
import me.jtalk.home.configuration.Constants;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.ZoneId;
import java.time.ZonedDateTime;

@Entity
@EqualsAndHashCode(of = "id")
public class ArticleComment {

	@Id
	private String id;

	@NotNull
	@Size(min = 1, max = Constants.MAX_COMMENT_AUTHOR_NAME_SIZE)
	@Column(length = Constants.MAX_COMMENT_AUTHOR_NAME_SIZE, updatable = false, nullable = false)
	private String author;

	@Column
	private ZonedDateTime posted;

	@Column
	private boolean owners = false;

	@NotNull
	@Size(min = 1, max = Constants.MAX_COMMENT_SIZE)
	@Column(length = Constants.MAX_COMMENT_SIZE, updatable = false, nullable = false)
	private String content;

	@NotNull
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "\"articleId\"", nullable = false, updatable = false)
	private Article article;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public ZonedDateTime getPosted() {
		return posted;
	}

	public void setPosted(ZonedDateTime posted) {
		this.posted = posted;
	}

	public boolean isOwners() {
		return owners;
	}

	public void setOwners(boolean owners) {
		this.owners = owners;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	@PrePersist
	public void onCreate() {
		ZonedDateTime now = ZonedDateTime.now(ZoneId.of("UTC"));
		setPosted(now);
	}
}
