/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.configuration;

public class Constants {
	public static final int OWNER_DATA_KEY_SIZE = 512;
	public static final int OWNER_DATA_VALUE_SIZE = 70000;
	public static final int MAX_PHOTO_SIZE = 50 * 1024 * 1024; // 50 MB

	public static final int MAX_PROJECT_NAME = 255;
	public static final int MAX_PROJECT_DESCRIPTION = 70000;
	public static final int MAX_PROJECT_LOGO_SIZE = 50 * 1024 * 1024; // 50 MB

	public static final int MAX_ARTICLE_SIZE = 70000;

	public static final int MAX_COMMENT_AUTHOR_NAME_SIZE = 255;
	public static final int MAX_COMMENT_SIZE = 2000;

	public static final int MAX_UPLOADED_IMAGE_DESCRIPTION = 255;
	public static final int MAX_UPLOADED_IMAGE_SIZE = 50 * 1024 * 1024; // 50 MB

	public static final String OWNER_USER_NAME_KEY = "userName";
	public static final String OWNER_USER_NICKNAME_KEY = "userNickname";
	public static final String OWNER_USER_EMAIL_KEY = "userEmail";
	public static final String OWNER_USER_DESCRIPTION_KEY = "userDescription";
	public static final String OWNER_USER_BIO_KEY = "userBio";

	public static final String JPA_FETCH_GRAPH_PARAM = "javax.persistence.fetchgraph";
}
