/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service

import javax.annotation.security.{RolesAllowed, PermitAll}
import javax.ejb.{TransactionAttributeType, TransactionAttribute, LocalBean, Stateless}
import javax.persistence.criteria.{Expression, Selection, JoinType}
import javax.persistence.{PersistenceContext, EntityManager}
import java.util.{List => JList}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.configuration.Roles
import me.jtalk.home.model.{ArticleTag_, ArticleTag}
import me.jtalk.home.utils.QueryUtils
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption

@Stateless
@LocalBean
class TagService extends StrictLogging {

	@PersistenceContext
	private var em: EntityManager = _

	@PermitAll
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	def loadReadOnly(): JList[ArticleTag] = QueryUtils.findAll(em, classOf[ArticleTag])

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def createTag(name: String): Boolean = Option(name)
			.filterOr(em.find(classOf[ArticleTag], _) == null, n => logger.debug("Tag {} found, creation impossible", n))
			.change(logger.debug("Tag {} not found, continue with creation", _))
			.map(new ArticleTag(_))
			.change(em.persist _)
			.exists(_ => true)

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def deleteTag(name: String): Boolean = Option(name)
			.mapEnsure(em.find(classOf[ArticleTag], _), n => logger.debug("Tag {} not found, proceeding with deletion", n))
			.changeFalse(logger.debug("Tag {} found, deletion impossible", name))
			.change(_.getArticles.clear())
			.change(em.remove _)
			.exists(_ => true)

	@PermitAll
	def countArticles(tag: String): Long = {
		val cb = em.getCriteriaBuilder
		val query = cb.createQuery(classOf[Long])
		val tags = query.from(classOf[ArticleTag])
		val articles = tags.join(ArticleTag_.articles)
		query
		        .select(cb.count(tags).asInstanceOf[Selection[Long]])
		        .where(cb.equal(tags.get(ArticleTag_.name), tag).asInstanceOf[Expression[java.lang.Boolean]])
		em.createQuery(query).getSingleResult
	}
}
