/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service

import javax.annotation.ManagedBean
import javax.enterprise.context.ApplicationScoped
import javax.enterprise.event.{TransactionPhase, Observes}
import javax.inject.Inject

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.service.event.CommentAdded
import me.jtalk.home.utils.EmailUtils
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption
import org.apache.commons.configuration.Configuration

@ManagedBean
@ApplicationScoped
class CommentsNotificationService extends StrictLogging {

	private val OWNER_EMAIL_PARAM = "me.jtalk.home.notification.owner.email"

	// We'd rather use this setting as website may have many external URLs and it's up to
	// it's owner to decide which one he wants to see in E-Mail
	private val EMAIL_LINK_TEMPLATE_PARAM = "me.jtalk.home.notification.url.template"

	@Inject
	private var config: Configuration = _

	@Inject
	private var emailUtils: EmailUtils = _

	def notify(articleInternalTitle: String, commentAuthor: String, commentContents: String): Boolean = {
		val articleUrl = linkFor(articleInternalTitle)
		val text =
			<html>
				<head>
					<meta http-equiv=" Content-Type " content="text/html; charset=utf-8"/>
					<title>Home page comments notification E-Mail</title>
				</head>
				<body>
					A new comment has been sent for article
					<a href={articleUrl}>
						{articleUrl}
					</a>.
					<br/>
					Author:
					{commentAuthor}
					<br/>
					Contents:
					<pre>
						{commentContents}
					</pre>
				</body>
			</html>.toString()
		ownerEmail()
				.changeFalse(logger.warn("Owner E-Mail is not set, notification skipped"))
				.exists(addr => emailUtils.send(address = addr, subject = "New comment on home page", formatMessage = text, mime = "text/html"))
	}

	def onComment(@Observes(during = TransactionPhase.AFTER_SUCCESS) e: CommentAdded) = notify(
		articleInternalTitle = e.internalTitle,
		commentAuthor = e.author,
		commentContents = e.text)

	protected def ownerEmail(): Option[String] = Some(config)
			.mapEnsure(_.getString(OWNER_EMAIL_PARAM))

	protected def linkFor(internalTitle: String): String = config
			.getString(EMAIL_LINK_TEMPLATE_PARAM, "%s")
			.format(internalTitle)
}
