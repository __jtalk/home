/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service

import java.util.{ArrayList => JArrayList, HashSet => JHashSet}
import javax.annotation.security.{RolesAllowed, PermitAll}
import javax.ejb._
import javax.persistence.{EntityManager, PersistenceContext, PersistenceContextType, SynchronizationType}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.configuration.Roles
import me.jtalk.home.exception.NotFoundException
import me.jtalk.home.model.{Article, ArticleTag, Article_}
import me.jtalk.home.utils.QueryUtils
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption

import scala.collection.JavaConversions._

@Stateful
@LocalBean
@RolesAllowed(Array(Roles.ADMINISTRATOR))
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
class BlogPostEdit extends Serializable with StrictLogging {

	@PersistenceContext(`type` = PersistenceContextType.EXTENDED, synchronization = SynchronizationType.UNSYNCHRONIZED)
	private var em: EntityManager = _

	@PersistenceContext
	private var transactionalEm: EntityManager = _

	private var article: Option[Article] = _
	private var tags: JArrayList[String] = _

	def load(id: String): Boolean = {
		article = Option(em.find(classOf[Article], id))
		tags = article
				.map(_.getTags)
				.map(_.map(_.getName))
				.map(new JArrayList(_))
				.getOrElse(new JArrayList)
		article.isDefined
	}

	def loadByName(name: String): Boolean = {
		article = Option(name)
				.map(QueryUtils.findByUniqueField(em, classOf[Article], Article_.internalTitle, _))
				.filter(_.size > 0)
				.filterOr(_.size == 1, l => logger.error("Too many blog entries for name {}: {} found", name, l.size.toString))
				.map(_.get(0))
		tags = article
				.map(_.getTags)
				.map(_.map(_.getName))
				.map(new JArrayList(_))
				.getOrElse(new JArrayList)
		article.isDefined
	}

	def get() = article.getOrElse(throw new NotFoundException("Article not found"))
	def getTags() = tags
	def setTags(t: JArrayList[String]) = tags = t
	def internalTitle: String = article.map(_.getInternalTitle).getOrElse("<empty>")
	def isLoaded() = article.isDefined

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	def save(): Boolean = {
		val otherExists = Some(get().getInternalTitle)
				.map(QueryUtils.findByUniqueField(transactionalEm, classOf[Article], Article_.internalTitle, _))
				.filter(_.size > 0)
				.map(_.get(0))
				.filter(_.getId != get().getId)
				.exists(_ => true)
		if (otherExists) {
			logger.warn("New article name {} duplicates another one's", internalTitle)
			false
		} else {
			em.joinTransaction()
			saveTags()
			logger.info("Saving article {}", internalTitle)
			true
		}
	}

	def reset(): Unit = {
		logger.debug("Article editing bean for {} has been reset", internalTitle)
		article.foreach(em.detach _)
		tags = new JArrayList
		article = None
	}

	@Remove
	def remove(): Unit = article = None

	protected def saveTags(): Unit = {
		val tagsSet = tags.toSet
		article.foreach(a => {
			val removed = a.getTags.filterNot(ar => tagsSet.contains(ar.getName))
			removed.foreach(_.getArticles.remove(a))
			a.getTags.removeAll(removed)
			val loadedTags = tagsSet.map(this.loadOrCreateTag)
			a.getTags.addAll(loadedTags)
			loadedTags.foreach(_.getArticles.add(a))
		})
	}

	private def loadOrCreateTag(tagName: String): ArticleTag = Option(em.find(classOf[ArticleTag], tagName))
		.orElse(Some(createManagedTag(tagName)))
		.get

	private def createManagedTag(tagName: String): ArticleTag = {
		val result = new ArticleTag(tagName)
		em.persist(result)
		result
	}
}
