/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service

import java.util.UUID
import javax.annotation.security.{PermitAll, RolesAllowed}
import javax.ejb.{LocalBean, Stateless, TransactionAttribute, TransactionAttributeType}
import javax.enterprise.inject.Instance
import javax.inject.Inject
import javax.persistence.criteria.Expression
import javax.persistence.{EntityManager, PersistenceContext}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.configuration.Roles
import me.jtalk.home.model.{Project, Project_}
import me.jtalk.home.utils.scala.EnhancedBoolean._
import me.jtalk.home.utils.scala.EnhancedOption._

import scala.collection.JavaConversions._

@Stateless
@LocalBean
class ProjectService extends StrictLogging {

	@PersistenceContext
	private var em: EntityManager = _

	@Inject
	private var uuid: Instance[UUID] = _

	@PermitAll
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	def loadReadOnly(published: Boolean) = loadInternal(Some(published))

	@PermitAll
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	def loadReadOnly() = loadInternal(None)

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	def load(published: Boolean) = loadInternal(Some(published))

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	def load() = loadInternal(None)

	/**
	  * This moving implementation is slow and stupid but we don't expect too many projects to be
	  * handled by this service. This implementation is the best trade-off between fast development
	  * and features.
	  *
	  * @param id project ID to move.
	  *
	  * @return if move was successful.
	  */
	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def moveLeft(id: String): Boolean = {
		val all = load()
		val found = all.indexWhere(_.getId == id)
		Option(found)
				.filterOr(_ >= 0, _ => logger.warn("Project {} moving left error: no project found", id))
				.filterOr(_ > 0, _ => logger.warn("Project {} moving left skipped: this is the first project", id))
				.change(i => all.get(i).setOrder(all.get(i).getOrder - 1))
				.change(i => all.get(i - 1).setOrder(all.get(i - 1).getOrder + 1))
				.change(_ => logger.debug("Project {} has been moved left", id))
				.isDefined
	}

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def moveRight(id: String): Boolean = {
		val all = load()
		val found = all.indexWhere(_.getId == id)
		Option(found)
				.filterOr(_ >= 0, _ => logger.warn("Project {} moving right error: no project found", id))
				.filterOr(_ < all.size() - 1, _ => logger.warn("Project {} moving right skipped: this is the last project", id))
				.change(i => all.get(i).setOrder(all.get(i).getOrder + 1))
				.change(i => all.get(i + 1).setOrder(all.get(i + 1).getOrder - 1))
				.change(_ => logger.debug("Project {} has been moved right", id))
				.isDefined
	}

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def create(project: Project): Boolean = {
		if (project.getId == null) {
			project.setId(uuid.get.toString)
		}
		Option(project.getInternalName)
				.flatMapWith(findByInternalName _, logger.debug("Project {} not found, creation is possible", _))
				.map(_ => false)
				.orElse(Some(true))
				.change(if (_) {
					em.persist(project)
				})
				.get
	}

	protected def findByInternalName(name: String): Option[Project] = {
		val cb = em.getCriteriaBuilder
		val query = cb.createQuery(classOf[Project])
		val root = query.from(classOf[Project])
		query.where(cb.equal(root.get(Project_.internalName), name).asInstanceOf[Expression[java.lang.Boolean]])
		Option(em.createQuery(query).getResultList)
				.filter(!_.isEmpty)
				.map(_.get(0))
	}

	protected def loadInternal(published: Option[Boolean]) = {
		val cb = em.getCriteriaBuilder
		val query = cb.createQuery(classOf[Project])
		val root = query.from(classOf[Project])
		published
				.change(p => logger.debug("{} projects loading requested", p.decide("Published", "Unpublished")))
				.map(p => query.where(cb.equal(root.get(Project_.published), p).asInstanceOf[Expression[java.lang.Boolean]]))
				.map(_ => Unit)
				.getOrElse(logger.debug("All projects loading requested"))
		query.orderBy(cb.asc(root.get(Project_.order)))
		em.createQuery(query).getResultList
	}
}
