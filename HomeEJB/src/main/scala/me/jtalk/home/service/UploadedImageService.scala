/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service

import java.util
import java.util.{List => JList, UUID}
import javax.annotation.security.{RolesAllowed, PermitAll}
import javax.ejb.{LocalBean, Stateless, TransactionAttribute, TransactionAttributeType}
import javax.enterprise.inject.Instance
import javax.inject.Inject
import javax.persistence.{EntityManager, PersistenceContext}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.configuration.{Roles, Constants}
import me.jtalk.home.model.{UploadedImage, UploadedImage_}
import me.jtalk.home.utils.QueryUtils
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption
import org.apache.commons.lang3.ArrayUtils

import scala.collection.JavaConversions._

@Stateless
@LocalBean
class UploadedImageService extends StrictLogging {

	@PersistenceContext
	private var em: EntityManager = _

	@Inject
	private var uuid: Instance[UUID] = _

	@PermitAll
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	def findContentReadOnly(id: String): Option[UploadedImage] = loadWithGraph(id, UploadedImage.LOAD_CONTENT_GRAPH)

	@PermitAll
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	def findMetadataPageReadOnly(page: Int, pageSize: Int): JList[UploadedImage] = loadPageWithGraph(page, pageSize, UploadedImage.LOAD_METADATA_GRAPH)

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def add(description: String, content: Array[Byte]): String = {
		val image = new UploadedImage
		image.setId(uuid.get.toString)
		image.setDescription(description)
		image.setContent(ArrayUtils.clone(content))
		em.persist(image)
		image.getId
	}

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def remove(id: String): Boolean = Some(id)
			.mapEnsure(em.find(classOf[UploadedImage], _), logger.warn("Removal requested for a non-existent image {}", _))
			.change(em.remove _)
			.exists(_ => true)

	@PermitAll
	def count(): Long = QueryUtils.count(em, classOf[UploadedImage])

	protected def loadPageWithGraph(page: Int, pageSize: Int, graphName: String): JList[UploadedImage] = {
		val cb = em.getCriteriaBuilder
		val query = cb.createQuery(classOf[UploadedImage])
		val root = query.from(classOf[UploadedImage])
		val graph = em.getEntityGraph(graphName)
		query.orderBy(cb.desc(root.get(UploadedImage_.uploaded)))
		em.createQuery(query)
				.setHint(Constants.JPA_FETCH_GRAPH_PARAM, graph)
				.setFirstResult(page * pageSize)
				.setMaxResults(pageSize)
				.getResultList
	}

	protected def loadWithGraph(id: String, graphName: String): Option[UploadedImage] = {
		val graph = em.getEntityGraph(graphName)
		val props = Map[String, AnyRef]((Constants.JPA_FETCH_GRAPH_PARAM, graph))
		Some(id)
		        .mapEnsure(em.find(classOf[UploadedImage], _, props), logger.warn("Image {} not found while searched", _))
	}
}
