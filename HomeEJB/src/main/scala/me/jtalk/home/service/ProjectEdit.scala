/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service

import javax.annotation.security.{PermitAll, RolesAllowed}
import javax.ejb._
import javax.persistence.criteria.Expression
import javax.persistence.{EntityManager, PersistenceContext, PersistenceContextType, SynchronizationType}

import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.configuration.Roles
import me.jtalk.home.exception.NotFoundException
import me.jtalk.home.model.{Project, Project_}
import me.jtalk.home.utils.scala.EnhancedOption._
import me.jtalk.home.utils.scala.EnhancedAny._

@Stateful
@LocalBean
@RolesAllowed(Array(Roles.ADMINISTRATOR))
class ProjectEdit extends Serializable with StrictLogging {

	@PersistenceContext(`type` = PersistenceContextType.EXTENDED, synchronization = SynchronizationType.UNSYNCHRONIZED)
	private var em: EntityManager = _

	private var project: Option[Project] = None

	def load(id: String): Boolean = {
		project
				.map(p => logger.error("Project editor is requested to load a project with id {} while another project {} is already loaded", id, p.getInternalName))
		project = Option(em.find(classOf[Project], id))
		        .change(_ => logger.debug("Project with id {} opened for editing", id))
		        .changeFalse(logger.debug("Unable to open project with id {} for editing: not found", id))
		isLoaded
	}

	def loadByName(name: String): Boolean = {
		project
				.map(p => logger.error("Project editor is requested to load a project {} while another project {} is already loaded", name, p.getInternalName))
		val cb = em.getCriteriaBuilder
		val query = cb.createQuery(classOf[Project])
		val root = query.from(classOf[Project])
		query
				.where(cb.equal(root.get(Project_.internalName), name).asInstanceOf[Expression[java.lang.Boolean]])
		val found = em.createQuery(query).getResultList
		if (found.isEmpty) {
			logger.debug("Unable to open project with name {} for editing: not found", name)
			false
		} else {
			logger.debug("Project with name {} opened for editing", name)
			project = Some(found.get(0))
			true
		}
	}

	def get() = project.getOrElse(throw new NotFoundException("Project not found"))
	def internalName: String = project.map(_.getInternalName).getOrElse("<empty>")
	def isLoaded() = project.isDefined
	def save() = em.joinTransaction() withCall logger.info("Saving project {}", internalName)

	def reset(): Unit = {
		logger.debug("Project editing bean for {} has been reset", internalName)
		project.map(em.detach _)
		project = None
	}

	def delete(): Unit = {
		logger.debug("Project editing bean deletes project {}", internalName)
		em.joinTransaction()
		project.map(em.remove _)
		project = None
	}

	@Remove
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	def remove() = reset()
}
