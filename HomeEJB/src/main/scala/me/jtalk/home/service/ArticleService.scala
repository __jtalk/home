/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service

import java.util.UUID
import javax.annotation.security.{RolesAllowed, PermitAll}
import javax.ejb.{LocalBean, Stateless, TransactionAttribute, TransactionAttributeType}
import javax.enterprise.event.Event
import javax.enterprise.inject.Instance
import javax.inject.Inject
import javax.persistence.criteria.Expression
import javax.persistence.{EntityManager, PersistenceContext}

import com.codahale.metrics.annotation.Timed
import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.configuration.Roles
import me.jtalk.home.model.{Article, ArticleComment, ArticleTag_, Article_}
import me.jtalk.home.service.event.CommentAdded
import me.jtalk.home.utils.QueryUtils
import me.jtalk.home.utils.scala.EnhancedBoolean.enhanceBoolean
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption

import scala.collection.JavaConversions._

@Stateless
@LocalBean
class ArticleService extends StrictLogging {

	@PersistenceContext
	private var em: EntityManager = _

	@Inject
	private var uuid: Instance[UUID] = _

	@Inject
	private var onComment: Event[CommentAdded] = _

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def create(article: Article): Boolean = {
		if (article.getId == null) {
			article.setId(uuid.get().toString)
		} else if (em.find(classOf[Article], article.getId) != null) {
			return false;
		}
		em.persist(article)
		return true;
	}

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	@SuppressWarnings(Array("deprecated"))
	def create(title: String, internalTitle: String): Option[Article] = existByName(internalTitle)
			.unary_!
			.toOption
			.changeFalse(logger.debug("Duplicating article creation requested for name {}", internalTitle))
			.map(_ => new Article)
			.change(_.setId(uuid.get.toString))
			.change(_.setTitle(title))
			.change(_.setInternalTitle(internalTitle))
			.change(_.setContent(
				"""
				  |## This is an article header ##
				  |
				  | And this is a preview line.
				  |
				  | &cut&
				  |
				  | This this is a body where all the stuff comes to.
				""".stripMargin))
			.change(_.setPurifiedContent(""))
			.change(em.persist _)
			.change(a => logger.info("Article created with name ''{}''", a.getInternalTitle))

	@PermitAll
	@Timed(name = "articles.load.page.timer", absolute = true)
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	def loadPageReadOnly(page: Int, pageSize: Int) = {
		val cb = em.getCriteriaBuilder
		val query = cb.createQuery(classOf[Article])
		val root = query.from(classOf[Article])
		query
				.where(cb.isTrue(root.get(Article_.published)).asInstanceOf[Expression[java.lang.Boolean]])
				.orderBy(cb.desc(root.get(Article_.created)))
		val found = em.createQuery(query)
				.setFirstResult(page * pageSize)
				.setMaxResults(pageSize)
				.getResultList
		found
	}

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	@Timed(name = "articles.load.all.timer", absolute = true)
	def loadAll() = {
		val cb = em.getCriteriaBuilder
		val query = cb.createQuery(classOf[Article])
		val root = query.from(classOf[Article])
		query.orderBy(cb.desc(root.get(Article_.created)))
		em.createQuery(query).getResultList
	}

	@PermitAll
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	def loadReadOnly(id: String): Option[Article] = {
		val cb = em.getCriteriaBuilder
		val query = cb.createQuery(classOf[Article])
		val root = query.from(classOf[Article])
		query.where(cb.equal(root.get(Article_.id), id).asInstanceOf[Expression[java.lang.Boolean]])
		query.distinct(true)
		return Option(em.createQuery(query))
				.map(_.getResultList)
				.filter(_.size > 0)
				.map(_.get(0))
	}

	@PermitAll
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	def loadByNameReadOnly(name: String): Option[Article] = {
		val cb = em.getCriteriaBuilder
		val query = cb.createQuery(classOf[Article])
		val root = query.from(classOf[Article])
		query.where(cb.equal(root.get(Article_.internalTitle), name).asInstanceOf[Expression[java.lang.Boolean]])
		Option(em.createQuery(query))
				.map(_.getResultList)
				.filter(_.size > 0)
				.filterOr(_.size == 1, l => logger.error("Too many blog entries for name {}: {} found", name, l.size.toString))
				.map(_.get(0))
	}

	@PermitAll
	def existByName(name: String): Boolean = QueryUtils.exists(em, classOf[Article], Article_.internalTitle, name)

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def delete(id: String): Boolean = Option(id)
			.changeFalse(logger.error("No id provided to the article removal routine"))
			.flatMapWith(loadReadOnly _, logger.warn("Removal requested to the non-existent article {}", _))
			.change(a => a.getTags.foreach(_.getArticles.remove(a)))
			.map(em.remove _)
			.exists(_ => true)

	@PermitAll
	def addComment(articleId: String, author: String, content: String): Unit = {
		val article = em.find(classOf[Article], articleId)
		if (article == null) {
			throw new IllegalArgumentException(s"Article with id ${articleId} does not exist")
		}
		onComment.fire(new CommentAdded(
			internalTitle = article.getInternalTitle,
			author = author,
			text = content))
		val comment = new ArticleComment
		comment.setId(uuid.get.toString)
		comment.setAuthor(author)
		comment.setContent(content)
		comment.setArticle(article)
		article.getComments.add(comment)
		em.persist(comment)
	}

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def loadComment(id: String): Option[ArticleComment] = Option(id)
			.changeFalse(logger.error("No id provided to the comment search routine"))
			.mapEnsure(em.find(classOf[ArticleComment], _), id => logger.warn("Comment retrieval requested for a non-existent comment {}", id))

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def setCommentOwnerMark(id: String, mark: Boolean): Boolean = loadComment(id)
			.filterOr(_.isOwners != mark, _ => logger.debug("Changing comment owner mark for {} to {} not needed: already marked", id, mark.toString))
			.change(_.setOwners(mark))
			.change(_ => logger.debug("Comment {} has been marked as {}", id, mark.decide("owner's", "not owner's")))
			.exists(_ => true)

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	def removeComment(id: String): Boolean = loadComment(id)
			.change(em.remove _)
			.change(_ => logger.debug("Comment {} has been removed", id))
			.exists(_ => true)

	@PermitAll
	def countPublished(): Long = QueryUtils.countWith[Article, java.lang.Boolean](em, classOf[Article], Article_.published, true)

	@PermitAll
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	def search(toFind: String): List[Article] = {
		val queryPattern = QueryUtils.likePattern(toFind)
		val cb = em.getCriteriaBuilder
		val query = cb.createQuery(classOf[Article])
		val article = query.from(classOf[Article])
		val tags = article.join(Article_.tags)
		query
				.where(cb.or(
					cb.like(cb.lower(article.get(Article_.title)), queryPattern),
					cb.like(article.get(Article_.purifiedContent), queryPattern),
					cb.like(cb.lower(tags.get(ArticleTag_.name)), queryPattern)
				).asInstanceOf[Expression[java.lang.Boolean]])
				.orderBy(cb.desc(article.get(Article_.created)))
		query.distinct(true)
		em.createQuery(query).getResultList.toList
	}
}
