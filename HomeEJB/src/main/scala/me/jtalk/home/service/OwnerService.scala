/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.service

import javax.annotation.security.{PermitAll, RolesAllowed}
import javax.ejb.{Stateless, TransactionAttribute, TransactionAttributeType}
import javax.persistence.{EntityManager, PersistenceContext}

import me.jtalk.home.configuration.{Constants, Roles}
import me.jtalk.home.model.{SpamSettings, Owner}

import scala.beans.BeanProperty
import scala.collection.JavaConversions._

@Stateless
class OwnerService {

	protected val defaults: Map[String, String] = Map(
		(Constants.OWNER_USER_NAME_KEY,
				"Roman Nazarenko"),
		(Constants.OWNER_USER_NICKNAME_KEY,
				"Jtalk"),
		(Constants.OWNER_USER_EMAIL_KEY,
				"me@jtalk.me"),
		(Constants.OWNER_USER_DESCRIPTION_KEY,
				"""
				  |Java EE developer, free software enthusiast
				""".stripMargin),
		(Constants.OWNER_USER_BIO_KEY,
				"""
				  |# Bio #
				  |
				  |Sed ut *perspiciatis* unde -omnis- iste _natus_ error sit **voluptatem** accusantium doloremque laudantium, totam
				  |rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
				  |explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia
				  |consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui
				  |dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora
				  |incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum
				  |exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem
				  |vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum
				  |qui dolorem eum fugiat quo voluptas nulla pariatur?
				  |
				  |# Professional #
				  |
				  |But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born
				  |and I will give you a complete account of the system, and expound the actual teachings of the great
				  |explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids
				  |pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure
				  |rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or
				  |pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances
				  |occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us
				  |ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any
				  |right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one
				  |who avoids a pain that produces no resultant pleasure?
				""".stripMargin)
	)

	@PersistenceContext
	@BeanProperty
	var em: EntityManager = _

	@RolesAllowed(Array(Roles.ADMINISTRATOR))
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	def load(): Owner = {
		var found = em.find(classOf[Owner], Owner.DEFAULT_ID)
		if (found == null) {
			found = new Owner();
			found.getEntries.putAll(defaults)
			found.setSpamSettings(new SpamSettings)
			found.getSpamSettings.setOwner(found)
			em.persist(found)
			em.persist(found.getSpamSettings)
		}
		return found
	}

	@PermitAll
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	def loadReadOnly(): Owner = {
		val withParamsGraph = em.getEntityGraph(Owner.GRAPH_WITH_ENTRIES)
		var found = em.find(classOf[Owner], Owner.DEFAULT_ID, Map((Constants.JPA_FETCH_GRAPH_PARAM, withParamsGraph)))
		if (found == null) {
			found = new Owner();
			found.getEntries.putAll(defaults)
			found.setSpamSettings(new SpamSettings)
			found.getSpamSettings.setOwner(found)
			em.persist(found)
			em.persist(found.getSpamSettings)
		}
		return found
	}
}
