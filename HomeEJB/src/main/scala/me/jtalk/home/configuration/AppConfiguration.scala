/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.configuration

import java.util
import java.util.Arrays
import java.util.concurrent.Callable
import javax.annotation.{PostConstruct, ManagedBean}
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject

import com.codahale.metrics.{Timer, Meter, Metric, MetricRegistry}
import com.codahale.metrics.annotation.{Counted, Metered}
import com.typesafe.scalalogging.StrictLogging
import org.apache.commons.configuration.{Configuration, CompositeConfiguration, SystemConfiguration, AbstractConfiguration}

@ManagedBean
@ApplicationScoped
class AppConfiguration extends AbstractConfiguration with StrictLogging {

	@Inject
	private var metrics: MetricRegistry = _

	private var base: Configuration = _
	private var propertyMetric: Meter = _
	private var containsMetric: Meter = _
	private var propertyTimer: Timer = _
	private var containsTimer: Timer = _

	@PostConstruct
	def init(): Unit = {
		val sysConfig = new SystemConfiguration
		base = new CompositeConfiguration(Arrays.asList(sysConfig))
		propertyMetric = metrics.meter(MetricRegistry.name("configuration", "get", "meter"))
		containsMetric = metrics.meter(MetricRegistry.name("configuration", "contains", "meter"))
		propertyTimer = metrics.timer(MetricRegistry.name("configuration", "get", "timer"))
		containsTimer = metrics.timer(MetricRegistry.name("configuration", "contains", "timer"))
		logger.info("Configuration created")
	}

	override def getProperty(key: String): AnyRef = {
		propertyMetric.mark()
		propertyTimer.time(new Callable[AnyRef] {
			override def call(): AnyRef = base.getProperty(key)
		})
	}

	override def containsKey(key: String): Boolean = {
		containsMetric.mark()
		containsTimer.time(new Callable[Boolean] {
			override def call(): Boolean = base.containsKey(key)
		})
	}

	override def addPropertyDirect(key: String, value: scala.Any): Unit = base.addProperty(key, value)
	override def getKeys: util.Iterator[String] = base.getKeys
	override def isEmpty: Boolean = base.isEmpty
}
