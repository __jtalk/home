/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.configuration

import javax.annotation.PostConstruct
import javax.ejb.{Singleton, Startup}
import javax.inject.Inject

import com.codahale.metrics.health.HealthCheck.Result
import com.codahale.metrics.health.{HealthCheck, HealthCheckRegistry}
import me.jtalk.home.service.OwnerService

import scala.util.Try

@Singleton
@Startup
class HealthCheckRegistration {

	@Inject
	private var checker: OwnerService = _

	@Inject
	private var registry: HealthCheckRegistry = _

	@PostConstruct
	def init(): Unit = {
		registry.register("database", new HealthCheck {
			override def check(): Result = Try(checker.loadReadOnly())
			        .map(_ => Result.healthy())
			        .recover({
						case e: Exception => Result.unhealthy(e)
					})
			        .get
		})
	}
}
