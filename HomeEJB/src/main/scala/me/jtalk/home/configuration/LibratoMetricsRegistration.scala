/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.configuration

import java.util
import java.util.concurrent.TimeUnit
import javax.annotation.{Resource, PostConstruct, ManagedBean, PreDestroy}
import javax.ejb.{TransactionManagementType, TransactionManagement, Startup, Singleton}
import javax.enterprise.concurrent.ManagedScheduledExecutorService
import javax.enterprise.context.{ApplicationScoped, Initialized}
import javax.enterprise.event.Observes
import javax.inject.Inject

import com.codahale.metrics.{Metric, MetricFilter, MetricRegistry}
import com.librato.metrics.LibratoReporter.MetricExpansionConfig
import com.librato.metrics.{LibratoReporter, ManagedLibratoReporter}
import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption
import org.apache.commons.configuration.Configuration

@Startup
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
class LibratoMetricsRegistration extends StrictLogging {

	val LIBRATO_USER_NAME_PARAM = "me.jtalk.home.metrics.librato.user"
	val LIBRATO_API_TOKEN_PARAM = "me.jtalk.home.metrics.librato.api.token"
	val LIBRATO_SOURCE_ID_PARAM = "me.jtalk.home.metrics.librato.source.id"

	val LIBRATO_INTERVAL_MILLIS_PARAM = "me.jtalk.home.metrics.librato.interval.millis"
	val LIBRATO_INTERVAL_MILLIS_DEFAULT = 60000

	@Inject
	private var metrics: MetricRegistry = _

	@Inject
	private var config: Configuration = _

	@Resource
	private var executor: ManagedScheduledExecutorService = _

	private var reporter: Option[ManagedLibratoReporter] = None

	@PostConstruct
	def init() {
		val name = loadParam(LIBRATO_USER_NAME_PARAM)
		val token = loadParam(LIBRATO_API_TOKEN_PARAM)
		val sourceId = loadParam(LIBRATO_SOURCE_ID_PARAM)
		val builder = for {
			n <- name
			t <- token
			s <- sourceId
		} yield {
			ManagedLibratoReporter.builder(metrics, n, t, s)
		}
		val updateIntervalMillis = config.getLong(LIBRATO_INTERVAL_MILLIS_PARAM, LIBRATO_INTERVAL_MILLIS_DEFAULT)
		reporter = builder
				.map(_.setExecutorService(executor))
				.map(_.setExpansionConfig(new MetricExpansionConfig(util.EnumSet.of(
					LibratoReporter.ExpandedMetric.COUNT,
					LibratoReporter.ExpandedMetric.RATE_15_MINUTE,
					LibratoReporter.ExpandedMetric.RATE_MEAN,
					LibratoReporter.ExpandedMetric.PCT_95
				))))
				.map(_.setFilter(new MetricFilter {
					private val FILTERED_PREFIXES = List[String](
						"filter.search.page",
						"filter.admin",
						"filter.resources",
						"filter.image",
						"search.bio",
						"search.blog",
						"search.projects"
					)
					override def matches(name: String, metric: Metric): Boolean = FILTERED_PREFIXES
							.filter(name.startsWith(_))
							.isEmpty
				}))
				.map(_.setOmitComplexGauges(false))
				.map(_.setDeleteIdleStats(true))
				.map(_.build())
				.change(_.start(updateIntervalMillis, TimeUnit.MILLISECONDS))
				.change(_ => logger.info("Librato registration completed"))
	}

	@PreDestroy
	def onStop(): Unit = {
		reporter
				.change(_.stop())
				.change(_ => logger.info("Librato reporter is stopped"))
				.changeFalse(logger.info("Librato reporter stopping skipped: not started"))
	}

	private def loadParam(name: String): Option[String] = Option(config)
			.mapEnsure(_.getString(name))
			.changeFalse(logger.warn("Librato registartion skipped: no property {}", name))
}
