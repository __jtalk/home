/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.utils

import java.awt.image.{BufferedImage, RenderedImage}
import java.awt.{Graphics2D, Image}
import java.io.{ByteArrayOutputStream, InputStream}
import javax.imageio.ImageIO

import me.jtalk.home.utils.scala.EnhancedTry._
import me.jtalk.home.utils.scala.ResourceManagement

import _root_.scala.languageFeature.implicitConversions
import _root_.scala.util.{Success, Try}

object ImageUtils {

	def toByteArrayOf(image: RenderedImage, outputType: String): Try[Array[Byte]] = {
		val out = new ByteArrayOutputStream()
		Success(image)
				.change(ImageIO.write(_, outputType, out))
		        .map(_ => out.toByteArray)
	}

	def toImage(input: InputStream, mime: String, resizeHeight: Int = -1, resizeWidth: Int = -1): Try[RenderedImage] = {
		val imgInput = ImageIO.createImageInputStream(input)
		findReaderFor(mime)
				.change(_.setInput(imgInput, true))
				.map(_.read(0))
		        .map(_.getScaledInstance(resizeWidth, resizeHeight, Image.SCALE_SMOOTH))
		        .map(toBufferedImage _)
	}

	def findReaderFor(mime: String) = Try(ImageIO.getImageReadersByMIMEType(mime))
			.filterWithException(_.hasNext, new IllegalArgumentException("Unknown mime type " + mime))
			.map(_.next)

	protected def toBufferedImage(orig: Image) = {
		val out = new BufferedImage(orig.getWidth(null), orig.getHeight(null), BufferedImage.TYPE_INT_ARGB)
		val renderer: Graphics2D = out.createGraphics()
		ResourceManagement.withAnyResource[Graphics2D, Any](
			renderer,
			r => r.drawImage(orig, 0, 0, null),
			_.dispose())
		out
	}
}
