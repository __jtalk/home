/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.utils.scala

import scala.language.implicitConversions

object EnhancedOption {

	@inline
	implicit def enhanceOption[T](v: Option[T]) = new EnhancedOptionImpl(v)

	class EnhancedOptionImpl[T](v: Option[T]) {

		@inline
		def mapEnsure[R](f: T => R): Option[R] = v.map(f).filter(_ != null)

		@inline
		def mapEnsure[R, F](f: T => R, onFail: T => F): Option[R] = flatMapWith(a => Option(f(a)), onFail)

		def flatMapWith[R, F](f: T => Option[R], onFail: T => F): Option[R] = {
			val result = v.flatMap(f)
			if (result.isEmpty && v.isDefined) {
				onFail(v.get)
			}
			result
		}

		@inline
		def change[R](f: T => R): Option[T] = {
			v.foreach(f)
			v
		}

		@inline
		def changeFalse[R](onNone: => R): Option[T] = {
			if (v.isEmpty) {
				onNone
			}
			v
		}

		@inline
		def filterOr[R](p: T => Boolean, orElse: T => R): Option[T] = {
			val result = v.filter(p)
			if (result.isEmpty && v.isDefined) {
				orElse(v.get)
			}
			result
		}
	}
}
