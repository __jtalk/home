/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package me.jtalk.home.utils

import javax.annotation.ManagedBean
import javax.enterprise.context.ApplicationScoped
import javax.inject.Inject
import javax.mail._
import javax.mail.internet.{InternetAddress, MimeMessage}
import javax.naming.{InitialContext, NamingException}

import com.codahale.metrics.MetricRegistry
import com.typesafe.scalalogging.StrictLogging
import me.jtalk.home.utils.scala.EnhancedOption.enhanceOption
import org.apache.commons.configuration.Configuration

@ManagedBean
@ApplicationScoped
class EmailUtils @Inject() (metrics: MetricRegistry) extends StrictLogging {

	private val EMAIL_SESSION_NAME_PARAM = "me.jtalk.home.email.session.address"
	private val EMAIL_SESSION_NAME_DEFAULT = "java:/mail/home/session"

	@Inject
	private var config: Configuration = _

	private val sentCounter = Option(metrics).map(_.counter("email-utils.send.count")).orNull
	private val activeCounter = Option(metrics).map(_.counter("email-utils.send.active")).orNull

	def this() = this(null)

	def send(address: String, subject: String, formatMessage: String, mime: String): Boolean = {
		sentCounter.inc()
		activeCounter.inc()
		try {
			sendImpl(
				address = address,
				subject = subject,
				formatMessage = formatMessage,
				mime = mime)
		} finally {
			activeCounter.dec()
		}
	}

	private def sendImpl(address: String, subject: String, formatMessage: String, mime: String): Boolean = {
		try {
			val session = getSession
			return session.map(new MimeMessage(_))
					.change(_.setFrom)
					.change(_.setSubject(subject))
					.change(_.setContent(formatMessage, mime))
					.change(_.setRecipient(Message.RecipientType.TO, new InternetAddress(address)))
					.change(Transport.send _)
					.changeFalse(logger.debug("E-Mail notification sent, but no session configured"))
					.exists(_ => true)
		}
		catch {
			case e: SendFailedException => {
				logger.error("Sending failed for address {}", address, e)
				return false
			}
			case e: Any => {
				logger.error("Error while sending E-Mail to {}", address, e)
				throw new IllegalStateException(e)
			}
		}
	}

	protected def getOutgoingAddressIfExist: Option[String] = {
		try {
			val session = getSession
			return session
					.mapEnsure(InternetAddress.getLocalAddress _)
					.mapEnsure(_.getAddress)
		}
		catch {
			case e: NamingException => {
				logger.debug("Outgoing address lookup failed: no session in context")
				return None
			}
		}
	}

	protected def getSessionAddress: String = config
			.getString(EMAIL_SESSION_NAME_PARAM, EMAIL_SESSION_NAME_DEFAULT)

	protected def getSession: Option[Session] = Some(new InitialContext())
			.mapEnsure(_.lookup(getSessionAddress))
			.map(_.asInstanceOf[Session])
}