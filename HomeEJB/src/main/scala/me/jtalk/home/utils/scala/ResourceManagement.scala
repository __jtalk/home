/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.utils.scala

object ResourceManagement {

	def withCloseableResource[T <: AutoCloseable, R](resource: T, action: T => R): R = withAnyResource[T, R](resource, action, _.close)

	def withAnyResource[T, R](resource: T, action: T => R, closer: T => Any): R = {
		var caught: Option[Exception] = None
		try {
			return action(resource)
		} catch {
			case e: Exception => {
				caught = Some(e)
				throw e
			}
		} finally {
			try {
				closer(resource)
			} catch {
				case e: Exception => {
					caught match {
						case Some(sup) => e.addSuppressed(sup)
						case _ => Unit
					}
					throw e
				}
			}
		}
	}
}
