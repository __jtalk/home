/*
 * Copyright (c) 2016 Roman Nazarenko.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package me.jtalk.home.utils.scala

import scala.language.implicitConversions
import scala.util.{Failure, Try}

object EnhancedTry {

	@inline
	implicit def enhanceTry[U](t: Try[U]) = new EnhancedTryImpl(t)

	class EnhancedTryImpl[U](t: Try[U]) {

		@inline
		def filterWithException(p: U => Boolean, orElse: => Throwable) = if (t.isSuccess && p(t.get)) {
			t
		} else {
			Failure(orElse)
		}

		@inline
		def change[T](m: U => T) = {
			t.map(m)
			t
		}
	}

}
